DROP TABLE IF EXISTS users, addresses, categories, products, cartlines, carts;

--CREATE TABLES
CREATE TABLE IF NOT EXISTS categories
(
    id   INTEGER GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    name TEXT NOT NULL UNIQUE
);

CREATE INDEX IF NOT EXISTS categories_name_idx ON categories (name);

CREATE TABLE IF NOT EXISTS products
(
    id            INTEGER GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    name          TEXT  NOT NULL UNIQUE,
    price         FLOAT NOT NULL CHECK (price > 0),
    categories_id INTEGER REFERENCES categories (id)
);

CREATE INDEX IF NOT EXISTS products_name_idx ON products (name);
CREATE INDEX IF NOT EXISTS products_price_idx ON products (price);
CREATE INDEX IF NOT EXISTS products_categories_id_idx ON products (categories_id);

CREATE TABLE IF NOT EXISTS addresses
(
    id         INTEGER GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    street     TEXT NOT NULL,
    complement TEXT,
    zip        TEXT NOT NULL,
    city       TEXT NOT NULL,
    country    TEXT NOT NULL
);

CREATE TABLE IF NOT EXISTS users
(
    id           INTEGER GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    firstname    TEXT NOT NULL,
    lastname     TEXT NOT NULL,
    pseudonym    TEXT NOT NULL UNIQUE,
    addresses_id INTEGER REFERENCES addresses (id)
);

CREATE INDEX IF NOT EXISTS users_firstname_idx ON users (firstname);
CREATE INDEX IF NOT EXISTS users_lastname_idx ON users (lastname);
CREATE INDEX IF NOT EXISTS users_pseudonym_idx ON users (pseudonym);

CREATE TABLE IF NOT EXISTS carts
(
    id        INTEGER GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    createdat TIMESTAMP NOT NULL DEFAULT now(),
    users_id  INTEGER REFERENCES users (id)
);


CREATE TABLE IF NOT EXISTS cartlines
(
    carts_id    INTEGER NOT NULL references carts (id),
    products_id INTEGER NOT NULL references products (id),
    quantity    INTEGER NOT NULL CHECK (quantity > 0),
    PRIMARY KEY (carts_id, products_id)
);

--DATA TEST CATEGORY
INSERT INTO categories
VALUES (default, 'Laptops');
INSERT INTO categories
VALUES (default, 'Desktops');
INSERT INTO categories
VALUES (default, 'Monitors');
INSERT INTO categories
VALUES (default, 'Servers');
INSERT INTO categories
VALUES (default, 'Storage');
INSERT INTO categories
VALUES (default, 'Networking');
INSERT INTO categories
VALUES (default, 'Accessories');

--DATA TEST PRODUCTS CAT.1 LAPTOPS
INSERT INTO products
VALUES (default, 'Latitude 3250', 939.01, 1);
INSERT INTO products
VALUES (default, 'XPS 13', 1119.00, 1);
INSERT INTO products
VALUES (default, 'Vostro 3500', 599.00, 1);
INSERT INTO products
VALUES (default, 'Inspiron 15', 679.00, 1);
INSERT INTO products
VALUES (default, 'Precision 5760', 3299.00, 1);
INSERT INTO products
VALUES (default, 'Chromebook 3100', 219.00, 1);
INSERT INTO products
VALUES (default, 'Alienware M15 R6', 1429.00, 1);
--DATA TEST PRODUCTS CAT.2 DESKTOPS
INSERT INTO products
VALUES (default, 'Optiplex 3080', 709.00, 2);
INSERT INTO products
VALUES (default, 'Vostro 3681', 569.00, 2);
INSERT INTO products
VALUES (default, 'Inspiron 24', 589.00, 2);
INSERT INTO products
VALUES (default, 'XPS Tower', 919.00, 2);
INSERT INTO products
VALUES (default, 'Precision 3450', 1119.00, 2);
INSERT INTO products
VALUES (default, 'Alienware Aurora R12', 1119.00, 2);
--DATA TEST PRODUCTS CAT.3 MONITORS
INSERT INTO products
VALUES (DEFAULT, 'UltraSharp 43 4K', 899.99, 3);
INSERT INTO products
VALUES (DEFAULT, 'Dell P2722H', 349.99, 3);
INSERT INTO products
VALUES (DEFAULT, 'Dell S2721HSX', 259.99, 3);
INSERT INTO products
VALUES (DEFAULT, 'Dell E2422H', 279.99, 3);
INSERT INTO products
VALUES (DEFAULT, 'Dell C6522QT', 3374.99, 3);
--DATA TEST PRODUCTS CAT.4 SERVERS
INSERT INTO products
VALUES (DEFAULT, 'PowerEdge T140 Power Server', 599.00, 4);
INSERT INTO products
VALUES (DEFAULT, 'PowerEdge R240 Rack Server', 639.00, 4);
INSERT INTO products
VALUES (DEFAULT, 'PowerEdge XR11 Rack Server', 3989.00, 4);
INSERT INTO products
VALUES (DEFAULT, 'PowerEdge M640 Blade Server', 1989.00, 4);
--DATA TEST PRODUCTS CAT.5 STORAGE
INSERT INTO products
VALUES (DEFAULT, 'PowerVault ME4 Series', 10429.00, 5);
INSERT INTO products
VALUES (DEFAULT, 'PowerVault NX Series Network-Attached Appliances', 3309.00, 5);
INSERT INTO products
VALUES (DEFAULT, 'PowerVault ME484 Storage Expansion Enclosure', 49789.00, 5);
INSERT INTO products
VALUES (DEFAULT, 'Dell PowerVault RD1000 Removable Disk Storage', 989.00, 5);
INSERT INTO products
VALUES (DEFAULT, 'Dell EMC PowerProtect DP4400 Appliance', 13929.00, 5);
--DATA TEST PRODUCTS CAT.6 NETWORKING
INSERT INTO products
VALUES (DEFAULT, 'Dell EMC PowerSwitch N3200 Series', 4429.00, 6);
INSERT INTO products
VALUES (DEFAULT, 'Dell EMC Networking Virtual Edge Platform 1405', 1719.00, 6);
INSERT INTO products
VALUES (DEFAULT, 'Dell EMC PowerSwitch N3208PX-ON', 4249.00, 6);
--DATA TEST PRODUCTS CAT.7 ACCESSORIES
INSERT INTO products
VALUES (DEFAULT, 'Dell Docking Station', 155.99, 7);
INSERT INTO products
VALUES (DEFAULT, 'Dell Pro Wireless Keyboard and Mouse – KM5221W', 44.99, 7);
INSERT INTO products
VALUES (DEFAULT, 'Xerox Phaser 6510/DNI Color Laser Printer ', 469.00, 7);
INSERT INTO products
VALUES (DEFAULT, 'Dell UltraSharp Webcam', 199.99, 7);
INSERT INTO products
VALUES (DEFAULT, 'Dell Pro Stereo Headset - WH3022', 51.79, 7);
INSERT INTO products
VALUES (DEFAULT, 'Seagate 2TB External Hard Drive STEA2000400 Black', 72.99, 7);

--DATA TEST ADDRESSES
INSERT INTO addresses
VALUES (DEFAULT, '74A Rue de Paris', '1er étage', '35000', 'Rennes', 'FRANCE');
INSERT INTO addresses
VALUES (DEFAULT, '548 Market St', null, 'PMB 57274', 'San Francisco', 'USA');
INSERT INTO addresses
VALUES (DEFAULT, '5020 148th Ave NE', 'Redmond Woods', 'WA 98052', 'Washington', 'USA');
INSERT INTO addresses
VALUES (DEFAULT, '2300 Oracle Way', null, 'TX 78741', 'Austin', 'USA');
INSERT INTO addresses
VALUES (DEFAULT, '7 Rue Claude Chappe', 'Rennes Atalante Champs Blancs', '35510', 'Cesson-Sévigné', 'FRANCE');

--DATA TEST USERS
INSERT INTO users
VALUES (DEFAULT, 'Alexandre', 'Baron', 'Alex', 1);
INSERT INTO users
VALUES (DEFAULT, 'Linus', 'Torvalds', 'Lino', 2);
INSERT INTO users
VALUES (DEFAULT, 'Bill', 'Gates', 'Bibi', 3);
INSERT INTO users
VALUES (DEFAULT, 'James', 'Gosling', 'Jamy', 4);