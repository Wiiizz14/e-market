package com.example.spring.repositories.specs;

import com.example.spring.models.User;
import org.springframework.data.jpa.domain.Specification;

import static com.example.spring.repositories.specs.SpecificationUtils.hasAttributeEquals;

/**
 * {@link Specification)s for {@link User } entity}.
 */
public class UserSpecifications {

    public static Specification<User> hasFirstName(String firstname) {
        return hasAttributeEquals("firstname", firstname);
    }

    public static Specification<User> hasLastName(String lastname) {
        return hasAttributeEquals("lastname", lastname);
    }

    public static Specification<User> hasPseudonym(String pseudonym) {
        return hasAttributeEquals("pseudonym", pseudonym);
    }
}
