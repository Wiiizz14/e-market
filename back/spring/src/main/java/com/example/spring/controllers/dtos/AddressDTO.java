package com.example.spring.controllers.dtos;

import com.example.spring.models.Address;
import lombok.Data;

@Data
public class AddressDTO {

    private Integer id;
    private String street;
    private String complement;
    private String zip;
    private String city;
    private String country;

    public static AddressDTO fromDomain(Address address) {
        AddressDTO addressDTO = new AddressDTO();
        addressDTO.setId(address.getId());
        addressDTO.setStreet(address.getStreet());
        addressDTO.setComplement(address.getComplement());
        addressDTO.setZip(address.getZip());
        addressDTO.setCity(address.getCity());
        addressDTO.setCountry(address.getCountry());
        return addressDTO;
    }

    public Address toDomain() {
        Address address = new Address();
        address.setId(this.id);
        address.setStreet(this.street);
        address.setComplement(this.complement);
        address.setZip(this.zip);
        address.setCity(this.city);
        address.setCountry(this.country);
        return address;
    }
}
