package com.example.spring.controllers;


import com.example.spring.controllers.dtos.UserDTO;
import com.example.spring.errors.FonctionalException;
import com.example.spring.repositories.UserRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/profiles")
@CrossOrigin
public class ProfileController {

    private final UserRepository userRepository;

    public ProfileController(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @GetMapping("/me")
    public ResponseEntity<UserDTO> getMe() {

        //Get user in context
        User whoAmI = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        //Check in database if present
        UserDTO userDTO = UserDTO.fromDomain(userRepository.getUserByPseudonym(whoAmI.getUsername())
                .orElseThrow(() -> new FonctionalException("User not found !")));

        return ResponseEntity.ok(userDTO);
    }
}
