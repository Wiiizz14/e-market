package com.example.spring.services;

import com.example.spring.controllers.dtos.UserDTO;
import com.example.spring.errors.NotFoundException;
import com.example.spring.models.User;
import com.example.spring.repositories.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@RequiredArgsConstructor
public class UserService {

    private final UserRepository userRepository;

    public User getUserById(Integer id) {
        return userRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("User not exist"));
    }

    @Transactional
    public User createUser(User user) {
        return userRepository.save(user);
    }

    @Transactional
    public User updateUser(Integer id, UserDTO userDTO) {
        User userToUpdate = userRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("User not exist"));

        userToUpdate.setFirstname(userDTO.getFirstname());
        userToUpdate.setLastname(userDTO.getLastname());
        userToUpdate.setPseudonym(userDTO.getPseudonym());

        return userToUpdate;
    }

    @Transactional
    public void deleteUser(Integer id) {
        User userToRemove = userRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("User not exist"));
        userRepository.delete(userToRemove);
    }
}
