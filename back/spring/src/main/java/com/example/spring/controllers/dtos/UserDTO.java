package com.example.spring.controllers.dtos;

import com.example.spring.models.User;
import lombok.Data;

@Data
public class UserDTO {

    private Integer id;
    private String firstname;
    private String lastname;
    private String pseudonym;
    private AddressDTO address;

    public static UserDTO fromDomain(User user) {
        UserDTO userDTO = new UserDTO();
        userDTO.setId(user.getId());
        userDTO.setFirstname(user.getFirstname());
        userDTO.setLastname(user.getLastname());
        userDTO.setPseudonym(user.getPseudonym());
        userDTO.setAddress(AddressDTO.fromDomain(user.getAddress()));
        return userDTO;
    }

    public User toDomain() {
        User user = new User();
        user.setId(this.id);
        user.setFirstname(this.firstname);
        user.setLastname(this.lastname);
        user.setPseudonym(this.pseudonym);
        user.setAddress(this.address.toDomain());
        return user;
    }
}
