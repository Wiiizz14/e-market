package com.example.spring.controllers.dtos;

import com.example.spring.models.CartLine;
import lombok.Data;

@Data
public class CartLineDTO {

    private ProductDTO product;
    private Integer quantity;
    private Float price;

    public static CartLineDTO fromDomain(CartLine cartLine) {
        return new CartLineDTO()
                .setProduct(ProductDTO.fromDomain(cartLine.getProduct()))
                .setQuantity(cartLine.getQuantity())
                .setPrice(cartLine.getPrice());
    }

    public CartLine toDomain() {
        CartLine cartLine = new CartLine();
        cartLine.setProduct(this.product.toDomain());
        cartLine.setQuantity(this.quantity);
        return cartLine;
    }
}