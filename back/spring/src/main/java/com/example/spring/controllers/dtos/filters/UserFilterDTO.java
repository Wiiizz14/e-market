package com.example.spring.controllers.dtos.filters;

import lombok.Data;

@Data
public class UserFilterDTO {

    private String firstname;
    private String lastname;
    private String pseudonym;
}
