package com.example.spring.controllers;

import com.example.spring.controllers.dtos.CategoryDTO;
import com.example.spring.errors.NotFoundException;
import com.example.spring.models.Category;
import com.example.spring.models.Product;
import com.example.spring.repositories.CategoryRepository;
import com.example.spring.repositories.ProductRepository;
import com.example.spring.services.CategoryService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@RestController
@CrossOrigin
public class CategoryController {

    private final CategoryRepository categoryRepository;
    private final CategoryService categoryService;
    private final ProductRepository productRepository;

    public CategoryController(CategoryRepository categoryRepository, CategoryService categoryService, ProductRepository productRepository) {
        this.categoryRepository = categoryRepository;
        this.categoryService = categoryService;
        this.productRepository = productRepository;
    }

    //Get All
    @GetMapping("/api/categories")
    public Set<CategoryDTO> getCategories() {
        List<Category> categories = categoryRepository.findAll();
        Set<CategoryDTO> categoriesDTO = new HashSet<>();
        for (Category c : categories) {
            Set<Product> products = productRepository.getProductsByCategoryId(c.getId());
            c.setProducts(products);
            categoriesDTO.add(CategoryDTO.fromDomain(c));
        }
        return categoriesDTO;
    }

    //Get by name
    @GetMapping("/api/categories/")
    public CategoryDTO getCategoryByName(@RequestParam String name) {
        return categoryRepository.getCategoryByName(name)
                .map(CategoryDTO::fromDomain)
                .orElseThrow(() -> new NotFoundException("Name not exist"));
    }

    //Get by id
    @GetMapping("/api/categories/{id}")
    public CategoryDTO getById(@PathVariable("id") Integer id) {
        return categoryRepository.findById(id)
                .map(CategoryDTO::fromDomain)
                .orElseThrow(() -> new NotFoundException("Id not exist"));
    }

    //Create
    @PostMapping("/api/categories")
    public ResponseEntity<Category> createCategory(@RequestBody CategoryDTO categoryDTO) {
        Category category = categoryService.createCategory(categoryDTO.toDomain());
        return ResponseEntity
                .created(URI.create("/api/categories/" + category.getId()))
                .body(category);
    }

    //Update
    @PutMapping("/api/categories/{id}")
    public CategoryDTO updateCategory(@PathVariable("id") Integer id, @RequestBody CategoryDTO categoryDTO) {
        return CategoryDTO.fromDomain(categoryService.updateCategory(id, categoryDTO));
    }

    //Delete
    @DeleteMapping("/api/categories/{id}")
    public ResponseEntity<String> deleteCategory(@PathVariable("id") Integer id) {
        categoryService.deleteCategory(id);
        return ResponseEntity
                .status(HttpStatus.NO_CONTENT)
                .body("Category deleted");
    }
}
