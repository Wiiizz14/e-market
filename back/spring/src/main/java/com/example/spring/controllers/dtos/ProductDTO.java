package com.example.spring.controllers.dtos;

import com.example.spring.models.Category;
import com.example.spring.models.Product;
import lombok.Data;

@Data
public class ProductDTO {

    private Integer id;
    private String name;
    private Float price;
    private Integer categoryId;

    public static ProductDTO fromDomain(Product product) {
        ProductDTO productDTO = new ProductDTO();
        productDTO.setId(product.getId());
        productDTO.setName(product.getName());
        productDTO.setPrice(product.getPrice());
        productDTO.setCategoryId(product.getCategory().getId());
        return productDTO;
    }

    public static ProductDTO fromDomainLight(Product product) {
        ProductDTO productDTO = new ProductDTO();
        productDTO.setId(product.getId());
        productDTO.setName(product.getName());
        productDTO.setPrice(product.getPrice());
        return productDTO;
    }

    public Product toDomain() {
        Product product = new Product();
        product.setId(this.id);
        product.setName(this.name);
        product.setPrice(this.price);

        Category category = new Category();
        category.setId(categoryId);
        product.setCategory(category);
        return product;
    }
}
