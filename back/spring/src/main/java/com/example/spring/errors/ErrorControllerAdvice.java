package com.example.spring.errors;

import com.example.spring.controllers.dtos.ErrorDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ErrorControllerAdvice {

    private static final Logger LOGGER = LoggerFactory.getLogger(ErrorControllerAdvice.class);

    @ExceptionHandler(NotFoundException.class)
    public ResponseEntity<ErrorDTO> handleNotFoundException(NotFoundException exception) {
        LOGGER.debug("Not found exception handling", exception);
        return ResponseEntity
                .status(HttpStatus.NOT_FOUND)
                .body(new ErrorDTO(exception.getMessage()));
    }

    @ExceptionHandler(BadRequestException.class)
    public ResponseEntity<ErrorDTO> handleBadRequestException(BadRequestException exception) {
        LOGGER.debug("Bad request exception handling", exception);
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body(new ErrorDTO(exception.getMessage()));
    }

    @ExceptionHandler(InstanceException.class)
    public ResponseEntity<ErrorDTO> handleInstanceException(InstanceException exception) {
        LOGGER.debug("Instance exception handling", exception);
        return ResponseEntity
                .status(HttpStatus.FORBIDDEN)
                .body(new ErrorDTO(exception.getMessage()));
    }

    @ExceptionHandler(FonctionalException.class)
    public ResponseEntity<ErrorDTO> handleFonctionalException(FonctionalException exception) {
        LOGGER.debug("Fonctional exception handling", exception);
        return ResponseEntity
                .status(HttpStatus.FORBIDDEN)
                .body(new ErrorDTO(exception.getMessage()));
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<ErrorDTO> handleUnexpectedException(Exception exception) {
        LOGGER.error("Unexpected Exception handling", exception);
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(new ErrorDTO(exception.getMessage()));
    }
}
