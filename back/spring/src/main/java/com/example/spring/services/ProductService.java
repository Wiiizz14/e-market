package com.example.spring.services;

import com.example.spring.controllers.dtos.ProductDTO;
import com.example.spring.errors.NotFoundException;
import com.example.spring.models.Category;
import com.example.spring.models.Product;
import com.example.spring.repositories.CategoryRepository;
import com.example.spring.repositories.ProductRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@RequiredArgsConstructor
public class ProductService {

    private final ProductRepository productRepository;
    private final CategoryRepository categoryRepository;

    @Transactional
    public Product createProduct(Product product) {
        Category existingCategory = categoryRepository.findById(product.getCategory().getId())
                .orElseThrow(() -> new NotFoundException("Category not exist"));
        product.setCategory(existingCategory);
        return productRepository.save(product);
    }

    @Transactional
    public Product updateProduct(Integer id, ProductDTO productDTO) {
        Product productToUpdate = productRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("Product not exist"));

        productToUpdate.setName(productDTO.getName());
        productToUpdate.setPrice(productDTO.getPrice());

        Category existingCategory = categoryRepository.findById(productDTO.getCategoryId())
                .orElseThrow(() -> new NotFoundException("Category not exist"));

        productToUpdate.setCategory(existingCategory);
        return productToUpdate;
    }

    @Transactional
    public void deleteProduct(Integer id) {
        Product productToRemove = productRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("Product not exist"));
        productRepository.delete(productToRemove);
    }
}
