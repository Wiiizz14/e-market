package com.example.spring.controllers.dtos.filters;

import lombok.Data;

@Data
public class ProductFilterDTO {

    private String name;
    private Integer price;
    private Integer priceMin;
    private Integer priceMax;
    private String category;
}
