package com.example.spring.controllers;

import com.example.spring.controllers.dtos.CartDTO;
import com.example.spring.controllers.dtos.PageDTO;
import com.example.spring.controllers.dtos.UserDTO;
import com.example.spring.controllers.dtos.filters.UserFilterDTO;
import com.example.spring.errors.NotFoundException;
import com.example.spring.models.User;
import com.example.spring.repositories.CartRepository;
import com.example.spring.repositories.UserRepository;
import com.example.spring.services.UserService;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.web.SortDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static com.example.spring.repositories.specs.UserSpecifications.*;


@RestController
@CrossOrigin
public class UserController {

    private final UserRepository userRepository;
    private final CartRepository cartRepository;
    private final UserService userService;

    public UserController(UserRepository userRepository, CartRepository cartRepository, UserService userService) {
        this.userRepository = userRepository;
        this.cartRepository = cartRepository;
        this.userService = userService;
    }

    private static Specification<User> fromFilter(UserFilterDTO filter) {
        return hasFirstName(filter.getFirstname())
                .and(hasLastName(filter.getLastname()))
                .and(hasPseudonym(filter.getPseudonym()));
    }

    //Get All
    @GetMapping("/api/users")
    public PageDTO<UserDTO> getUsers(UserFilterDTO userFilterDTO, @SortDefault(value = {"id"}) Pageable pageable) {
        Specification<User> userSpecification = fromFilter(userFilterDTO);
        return PageDTO.fromDomain(userRepository.findAll(userSpecification, pageable), UserDTO::fromDomain);
    }

    //Get by id
    @GetMapping("/api/users/{id}")
    public UserDTO getUserById(@PathVariable("id") Integer id) {
        return userRepository.findById(id)
                .map(UserDTO::fromDomain)
                .orElseThrow(() -> new NotFoundException("Id not exist"));
    }

    //Get cart by user id
    @GetMapping("/api/users/{id}/cart")
    public CartDTO getCartByUserId(@PathVariable("id") Integer id){
        return cartRepository.findCartByUserId(id)
                .map(CartDTO::fromDomain)
                .orElseThrow(() -> new NotFoundException("cart not exist"));
    }

    //Create
    @PostMapping("/api/users")
    public ResponseEntity<User> createUser(@RequestBody UserDTO userDTO) {
        User user = userService.createUser(userDTO.toDomain());
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(user);
    }

    //Update
    @PutMapping("/api/users/{id}")
    public UserDTO updateUser(@PathVariable("id") Integer id, @RequestBody UserDTO userDTO) {
        return UserDTO.fromDomain(userService.updateUser(id, userDTO));
    }

    //Delete
    @DeleteMapping("/api/users/{id}")
    public ResponseEntity<Void> deleteUser(@PathVariable("id") Integer id) {
        userService.deleteUser(id);
        return ResponseEntity
                .status(HttpStatus.NO_CONTENT)
                .body(null);
    }
}
