package com.example.spring.repositories.specs;

import com.example.spring.models.Product;
import org.springframework.data.jpa.domain.Specification;

import static com.example.spring.repositories.specs.SpecificationUtils.*;

/**
 * {@link Specification)s for {@link Product} entity}.
 */
public class ProductSpecifications {

    public static Specification<Product> hasName(String name) {
        return hasAttributeEquals("name", name);
    }

    public static Specification<Product> hasPrice(Integer price) {
        return hasAttributeEquals("price", price);
    }

    public static Specification<Product> hasPriceMin(Integer priceMin) {
        return hasAttributeGreaterThanOrEqualsTo("price", priceMin);
    }

    public static Specification<Product> hasPriceMax(Integer priceMax) {
        return hasAttributeLessThan("price", priceMax);
    }

    public static Specification<Product> hasCategory(String category) {
        return hasAttributeEquals("category.name", category);
    }
}
