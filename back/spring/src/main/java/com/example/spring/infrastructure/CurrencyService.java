package com.example.spring.infrastructure;

import com.example.spring.models.Product;
import com.example.spring.repositories.ProductRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

@Service
public class CurrencyService {

    private final ProductRepository productRepository;
    private final CurrencyApiConverter currencyApiConverter;

    public CurrencyService(ProductRepository productRepository, CurrencyApiConverter currencyApiConverter) {
        this.productRepository = productRepository;
        this.currencyApiConverter = currencyApiConverter;
    }

    public Page<Product> findAll(Specification<Product> productSpecification, Pageable pageable, String currency) {
        if (currency != null) {
            return productRepository.findAll(productSpecification, pageable)
                    .map(p -> convertProduct(p, currency));
        } else {
            return productRepository.findAll(productSpecification, pageable);
        }
    }

    private Product convertProduct(Product product, String currency) {
        float priceConverted = currencyApiConverter.convertProductPrice(product, currency);
        Product p = new Product();
        p.setId(product.getId());
        p.setName(product.getName());
        p.setPrice(priceConverted);
        p.setCategory(product.getCategory());
        return p;
    }
}
