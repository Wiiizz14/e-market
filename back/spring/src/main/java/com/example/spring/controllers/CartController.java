package com.example.spring.controllers;

import com.example.spring.controllers.dtos.CartDTO;
import com.example.spring.controllers.dtos.CartLineDTO;
import com.example.spring.errors.NotFoundException;
import com.example.spring.models.Cart;
import com.example.spring.models.CartLine;
import com.example.spring.repositories.CartRepository;
import com.example.spring.services.CartService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.Set;

@RestController
@CrossOrigin
public class CartController {

    private final CartRepository cartRepository;
    private final CartService cartService;

    public CartController(CartRepository cartRepository, CartService cartService) {
        this.cartRepository = cartRepository;
        this.cartService = cartService;
    }

    //Create cart
    @PostMapping("/api/carts")
    public ResponseEntity<Cart> addCart(@RequestBody CartDTO cartDTO) {
        Cart cartAdded = cartService.createCart(cartDTO);
        return ResponseEntity
                .created(URI.create("/api/carts/" + cartAdded.getId()))
                .body(cartAdded);
    }

    //Get all carts
    @GetMapping("/api/carts")
    public Set<CartDTO> getCarts() {
        return cartRepository.findAll().stream()
                .map(CartDTO::fromDomain)
                .collect(java.util.stream.Collectors.toSet());
    }

    //Get cart by id
    @GetMapping("/api/carts/{id}")
    public CartDTO getCartById(@PathVariable("id") Integer id) {
        Cart cart = cartRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("Id not exist"));

        return CartDTO.fromDomain(cart);
    }

    //Create or update cartLine
    @PostMapping("/api/carts/{id}/products/{productId}")
    public ResponseEntity<CartLineDTO> addCartLine(@PathVariable("id") Integer id, @PathVariable("productId") Integer productId, @RequestBody CartLineDTO cartLineDTO) {
        CartLine cartLine = cartService.saveCartLine(id, productId, cartLineDTO);
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(CartLineDTO.fromDomain(cartLine));
    }

    //Delete cartLine
    @DeleteMapping("/api/carts/{id}/products/{productId}")
    public ResponseEntity<Void> deleteCartLine(@PathVariable("id") Integer id, @PathVariable("productId") Integer productId) {
        cartService.deleteCartLine(id, productId);
        return ResponseEntity
                .status(HttpStatus.NO_CONTENT)
                .body(null);
    }


    //Delete cart
    @DeleteMapping("/api/carts/{id}")
    public ResponseEntity<Void> deleteCart(@PathVariable("id") Integer id) {
        cartService.deleteCart(id);
        return ResponseEntity
                .status(HttpStatus.NO_CONTENT)
                .body(null);
    }
}
