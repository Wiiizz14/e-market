package com.example.spring.repositories.specs;

import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Path;
import javax.persistence.criteria.Root;

/**
 * Utility class to facilitate {@link Specification} usage.
 */
public class SpecificationUtils {

    public static <V, E> Specification<E> hasAttributeEquals(String attributePath, V value) {
        if (value == null) {
            return emptySpecification();
        }

        return (root, query, criteriaBuilder) -> criteriaBuilder.equal(getPath(root, attributePath), value);
    }

    public static <V extends Comparable<? super V>, E> Specification<E> hasAttributeGreaterThanOrEqualsTo(String attributePath, V value) {
        if (value == null) {
            return emptySpecification();
        }

        return (root, query, criteriaBuilder) -> criteriaBuilder.greaterThanOrEqualTo(getPath(root, attributePath), value);
    }

    public static <V extends Comparable<? super V>, E> Specification<E> hasAttributeLessThan(String attributePath, V value) {
        if (value == null) {
            return emptySpecification();
        }

        return (root, query, criteriaBuilder) -> criteriaBuilder.lessThan(getPath(root, attributePath), value);
    }

    private static <E> Specification<E> emptySpecification() {
        return (root, query, criteriaBuilder) -> null;
    }

    /**
     * Get {@link Path} from an attribute path to handle nested attributes (e.g. '<code>category.id</code>').
     */
    private static <I, O> Path<O> getPath(Root<I> root, String attributePath) {
        Path<I> path = root;
        for (String part : attributePath.split("\\.")) {
            path = path.get(part);
        }
        return (Path<O>) path;
    }
}


