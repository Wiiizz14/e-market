package com.example.spring.controllers.dtos;

import com.example.spring.models.Category;
import lombok.Data;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Data
public class CategoryDTO {

    private Integer id;
    private String name;
    private Set<ProductDTO> products = new HashSet<>();

    public static CategoryDTO fromDomain(Category category) {
        CategoryDTO categoryDTO = new CategoryDTO();
        categoryDTO.setId(category.getId());
        categoryDTO.setName(category.getName());
        categoryDTO.setProducts(category.getProducts()
                .stream()
                .map(ProductDTO::fromDomainLight)
                .collect(Collectors.toSet()));
        return categoryDTO;
    }

    public Category toDomain() {
        Category category = new Category();
        category.setId(this.id);
        category.setName(this.name);
        category.setProducts(this.getProducts()
                .stream()
                .map(ProductDTO::toDomain)
                .collect(Collectors.toSet()));
        return category;
    }
}
