package com.example.spring.errors;

public class FonctionalException extends RuntimeException {

    public FonctionalException() {
    }

    public FonctionalException(String message) {
        super(message);
    }

    public FonctionalException(String message, Throwable cause) {
        super(message, cause);
    }

    public FonctionalException(Throwable cause) {
        super(cause);
    }

    public FonctionalException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
