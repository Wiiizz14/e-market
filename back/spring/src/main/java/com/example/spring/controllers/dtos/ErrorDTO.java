package com.example.spring.controllers.dtos;

import lombok.Data;

import java.time.Instant;

@Data
public class ErrorDTO {
    private Instant timestamp = Instant.now();
    private String message;

    public ErrorDTO() {
    }

    public ErrorDTO(String message) {
        this.message = message;
    }
}
