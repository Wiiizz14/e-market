package com.example.spring.models;

/**
 * Convert a {@link Product} price into another currency.
 */

public interface CurrencyConverter {

    /**
     * Convert a {@link Product} price into another currency.
     *
     * @param product  to convert
     * @param currency to select change rate
     * @return Price converted
     */
    Float convertProductPrice(Product product, String currency);
}
