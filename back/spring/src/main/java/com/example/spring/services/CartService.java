package com.example.spring.services;

import com.example.spring.controllers.dtos.CartDTO;
import com.example.spring.controllers.dtos.CartLineDTO;
import com.example.spring.errors.InstanceException;
import com.example.spring.errors.NotFoundException;
import com.example.spring.models.Cart;
import com.example.spring.models.CartLine;
import com.example.spring.models.Product;
import com.example.spring.models.User;
import com.example.spring.repositories.CartRepository;
import com.example.spring.repositories.ProductRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@RequiredArgsConstructor
public class CartService {

    private final CartRepository cartRepository;
    private final ProductRepository productRepository;
    private final UserService userService;

    @Transactional
    public Cart createCart(CartDTO cartDTO) {
        User user = userService.getUserById(cartDTO.getUserId());
        if (cartRepository.findCartByUserId(user.getId()).isPresent()) {
            throw new InstanceException("Cart already exist for this user, id : " + user.getId());
        } else {
            Cart cart = new Cart(cartDTO.getUserId());
            return cartRepository.save(cart);
        }
    }

    @Transactional
    public CartLine saveCartLine(Integer id, Integer productId, CartLineDTO cartLineDTO) {
        Cart cart = cartRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("Cart not exist"));

        Product product = productRepository.findById(productId)
                .orElseThrow(() -> new NotFoundException("Product not exist"));

        return cart.addProduct(product, cartLineDTO.getQuantity());
    }

    @Transactional
    public void deleteCartLine(Integer cartId, Integer productId) {
        Cart cart = cartRepository.findById(cartId)
                .orElseThrow(() -> new NotFoundException("Cart not exist"));

        Product product = productRepository.findById(productId)
                .orElseThrow(() -> new NotFoundException("Product not exist"));

        CartLine cartLineToRemove = new CartLine(product, 0);
        cart.deleteCartLine(cartLineToRemove);
    }

    @Transactional
    public void deleteCart(Integer id) {
        Cart cart = cartRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("Cart not exist"));
        cartRepository.delete(cart);
    }
}
