package com.example.spring.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        PasswordEncoder encoder =
                PasswordEncoderFactories.createDelegatingPasswordEncoder();

        auth
                .inMemoryAuthentication()
                .withUser("Alex") //alexb
                .password("{bcrypt}$2a$10$v53PDagyGX1RnuhuMv.YkOika3KECUPM4cFZ2rzqAvpz.731FTAlm")
                .roles("USER","ADMIN")

                .and()
                .withUser("Lino") //linust
                .password("{bcrypt}$2a$10$h1fHNnv02jMaNkHR5fGWfuKguZ86x4SNgJpwHpOZ/2f2DJW0tdtvG")
                .roles("USER")

                .and()
                .withUser("Bibi") //billg
                .password("{bcrypt}$2a$10$803JwN8aP./H7HXuXy1JVOAQf8XbGbEr9KPt7uZyTDjNyuH4oMrnq")
                .roles("USER")

                .and()
                .withUser("Jamy") //jamesg
                .password("{bcrypt}$2a$10$nrGOHyCS7SJrh/tu5rvdse.Bt6nfuVFrqdhtcy0wSQYwC.Oap3pFC")
                .roles("USER");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .csrf().disable()
                .authorizeRequests()
                .antMatchers(HttpMethod.GET,"/api/categories").permitAll() //To display categories in navbar
                .antMatchers(HttpMethod.GET,"/api/categories/*").permitAll() //To display categories details
                .antMatchers(HttpMethod.GET,"/api/products").permitAll() //To display all products
                .antMatchers(HttpMethod.GET, "/api/products/*").permitAll() //To display product details
                .antMatchers(HttpMethod.GET, "/api/users/*").hasRole("USER")

                .antMatchers(HttpMethod.GET,"/api/carts").hasRole("ADMIN") //To display all carts
                .antMatchers(HttpMethod.POST, "/api/carts").hasAnyRole("ADMIN", "USER") //To add a cart

                .antMatchers(HttpMethod.POST, "/api/categories").hasRole("ADMIN") //To add a category
                .antMatchers(HttpMethod.POST, "/api/products").hasRole("ADMIN") //To add a product

                .antMatchers(HttpMethod.GET,"/api/users").hasRole("ADMIN") //To display all users
                .antMatchers(HttpMethod.POST, "/api/users").hasRole("ADMIN") //To add a user
                .antMatchers(HttpMethod.POST, "/api/users/*/cart").hasRole("USER")
                //Complete list with other paths following mappings in controllers
                .anyRequest().authenticated()
                .and()
                .httpBasic()
                .and()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .cors();
    }
}

