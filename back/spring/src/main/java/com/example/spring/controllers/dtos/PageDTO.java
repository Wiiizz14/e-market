package com.example.spring.controllers.dtos;

import lombok.Data;
import org.springframework.data.domain.Page;

import java.util.List;
import java.util.function.Function;

import static java.util.stream.Collectors.toList;

@Data
public class PageDTO<T> {

    private List<T> content;
    private int page;
    private int size;
    private int count;
    private long totalCount;
    private int totalPage;


    public static <I, O> PageDTO<O> fromDomain(Page<I> page, Function<I, O> domainToDtoMapper) {
        return new PageDTO<O>()
                .setContent(
                        page.getContent().stream()
                                .map(domainToDtoMapper)
                                .collect(toList()))
                .setPage(page.getPageable().getPageNumber() + 1) // Page index start at 1 in REST API but at 0 in Spring Data
                .setSize(page.getPageable().getPageSize())
                .setCount(page.getNumberOfElements())
                .setTotalPage(page.getTotalPages())
                .setTotalCount(page.getTotalElements());
    }
}
