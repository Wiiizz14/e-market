package com.example.spring.controllers;

import com.example.spring.controllers.dtos.PageDTO;
import com.example.spring.controllers.dtos.ProductDTO;
import com.example.spring.controllers.dtos.filters.ProductFilterDTO;
import com.example.spring.errors.NotFoundException;
import com.example.spring.infrastructure.CurrencyService;
import com.example.spring.models.Product;
import com.example.spring.repositories.ProductRepository;
import com.example.spring.services.ProductService;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.web.SortDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.HashSet;
import java.util.Set;

import static com.example.spring.repositories.specs.ProductSpecifications.*;

@RestController
@CrossOrigin
public class ProductController {

    private final ProductRepository productRepository;
    private final ProductService productService;
    private final CurrencyService currencyService;

    public ProductController(ProductRepository productRepository, ProductService productService, CurrencyService currencyService) {
        this.productRepository = productRepository;
        this.productService = productService;
        this.currencyService = currencyService;
    }

    private static Specification<Product> fromFilter(ProductFilterDTO filter) {
        return hasName(filter.getName())
                .and(hasPrice(filter.getPrice()))
                .and(hasPriceMin(filter.getPriceMin()))
                .and(hasPriceMax(filter.getPriceMax()))
                .and(hasCategory(filter.getCategory()));
    }

    //Get all
    @GetMapping("/api/products")
    public PageDTO<ProductDTO> getProducts(
            ProductFilterDTO productFilterDTO,
            @RequestParam(required = false) String currency,
            @SortDefault(value = {"id"}) Pageable pageable) {
        Specification<Product> productSpecification = fromFilter(productFilterDTO);
        return PageDTO.fromDomain(currencyService.findAll(productSpecification, pageable, currency), ProductDTO::fromDomain);
    }

    //Get by id
    @GetMapping("/api/products/{id}")
    public ProductDTO getById(@PathVariable("id") Integer id, @RequestParam(required = false) String currency) {
        return productRepository.findById(id)
                .map(ProductDTO::fromDomain)
                .orElseThrow(() -> new NotFoundException("Id not exist"));
    }

    //Get by category
    @GetMapping("/api/products/categories/{categoryId}")
    public Set<ProductDTO> getByCategoryId(@PathVariable("categoryId") Integer id, @RequestParam(required = false) String currency) {
        Set<Product> products = productRepository.getProductsByCategoryId(id);
        Set<ProductDTO> productsDTO = new HashSet<>();
        for (Product p : products) {
            productsDTO.add(ProductDTO.fromDomain(p));
        }
        return productsDTO;
    }

    //Create
    @PostMapping("/api/products")
    public ResponseEntity<ProductDTO> createProduct(@RequestBody ProductDTO productDTO) {
        Product p = productService.createProduct(productDTO.toDomain());
        return ResponseEntity
                .created(URI.create("/api/products/" + p.getId()))
                .body(ProductDTO.fromDomain(p));
    }

    //Update
    @PutMapping("/api/products/{id}")
    public ProductDTO updateProduct(@PathVariable("id") Integer id, @RequestBody ProductDTO productDTO) {
        return ProductDTO.fromDomain(productService.updateProduct(id, productDTO));
    }

    //Delete
    @DeleteMapping("/api/products/{id}")
    public ResponseEntity<Void> deleteProduct(@PathVariable("id") Integer id) {
        productService.deleteProduct(id);
        return ResponseEntity
                .status(HttpStatus.NO_CONTENT)
                .body(null);
    }
}
