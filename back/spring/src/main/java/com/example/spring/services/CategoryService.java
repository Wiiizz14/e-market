package com.example.spring.services;

import com.example.spring.controllers.dtos.CategoryDTO;
import com.example.spring.errors.NotFoundException;
import com.example.spring.models.Category;
import com.example.spring.repositories.CategoryRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@RequiredArgsConstructor
public class CategoryService {

    private final CategoryRepository categoryRepository;

    @Transactional
    public Category createCategory(Category category) {
        return categoryRepository.save(category);
    }

    @Transactional
    public Category updateCategory(Integer id, CategoryDTO categoryDTO) {
        Category categoryToUpdate = categoryRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("Category not exist"));
        categoryToUpdate.setName(categoryDTO.getName());
        return categoryToUpdate;
    }

    @Transactional
    public void deleteCategory(Integer id) {
        Category categoryToRemove = categoryRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("Category not exist"));
        categoryRepository.delete(categoryToRemove);
    }
}
