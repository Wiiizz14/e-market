package com.example.spring.controllers.dtos;

import com.example.spring.models.Cart;
import lombok.Data;

import java.time.Instant;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Data
public class CartDTO {

    private Integer id;
    private Integer userId;
    private Instant createdAt;
    private Set<CartLineDTO> cartLines = new HashSet<>();
    private Float price;

    public static CartDTO fromDomain(Cart cart) {
        return new CartDTO()
                .setId(cart.getId())
                .setUserId(cart.getUserId())
                .setCreatedAt(cart.getCreatedAt())
                .setCartLines(cart.getCartLines()
                        .stream()
                        .map(CartLineDTO::fromDomain)
                        .collect(Collectors.toSet()))
                .setPrice(cart.getTotalPrice());
    }

    public Cart toDomain() {
        Cart cart = new Cart();
        cart.setId(this.id);
        cart.setUserId(this.userId);
        cart.setCreatedAt(this.createdAt);
        cart.setCartLines(this.getCartLines()
                .stream()
                .map(CartLineDTO::toDomain)
                .collect(Collectors.toSet()));
        return cart;
    }

}
