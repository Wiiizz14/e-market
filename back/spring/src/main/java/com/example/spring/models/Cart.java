package com.example.spring.models;


import com.example.spring.errors.BadRequestException;

import javax.persistence.*;
import java.time.Instant;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "carts")
public class Cart {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "createdat")
    private Instant createdAt;

    @Column(name = "users_id")
    private Integer userId;

    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "cartlines",
            joinColumns = @JoinColumn(name = "carts_id", referencedColumnName = "id"))

    private Set<CartLine> cartLines = new HashSet<>();

    public Cart(Integer userId){
        this.userId = userId;
        this.createdAt = Instant.now();
    }

    // For JPA
    @Deprecated
    public Cart() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Instant getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Instant createdAt) {
        this.createdAt = createdAt;
    }

    public Set<CartLine> getCartLines() {
        return cartLines;
    }

    public void setCartLines(Set<CartLine> cartLines) {
        this.cartLines = cartLines;
    }

    public CartLine getCartLineByProductId(Integer id) {
        for (CartLine cl : cartLines) {
            if (Objects.equals(cl.getProduct().getId(), id)) {
                return cl;
            }
        }
        return null;
    }

    public CartLine getCartLineByProduct(Product product) {
        for (CartLine cl : cartLines) {
            if (cl.getProduct().equals(product)) {
                return cl;
            }
        }
        return null;
    }

    public CartLine addProduct(Product product, int quantity) {
        CartLine cartLine = getCartLineByProduct(product);
        if (cartLine != null) {
            if (quantity <= 0) {
                throw new BadRequestException("Quantity must be positive !");

            } else if (cartLine.getProduct().getId().equals(product.getId())) {
                cartLine.setQuantity(cartLine.getQuantity() + quantity);
                return cartLine;
            }
        }

        CartLine newCartLine = new CartLine(product, quantity);
        getCartLines().add(newCartLine);
        return newCartLine;
    }

    public void deleteCartLine(CartLine cartLineToRemove) {
        cartLines.removeIf(cartLine -> cartLine.getProduct().getId().equals(cartLineToRemove.getProduct().getId()));
    }

    public float getTotalPrice() {
        float totalPrice = 0;
        for (CartLine cl : cartLines) {
            totalPrice += cl.getPrice();
        }
        return totalPrice;
    }
}