package com.example.spring.infrastructure;

import com.example.spring.models.CurrencyConverter;
import com.example.spring.models.Product;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

/**
 * {@link CurrencyConverter} implementation that use external web service CurrencyApi.
 */

@Component
public class CurrencyApiConverter implements CurrencyConverter {

    private final RestTemplate restTemplate;

    public CurrencyApiConverter(RestTemplateBuilder builder, CurrencyApiProperties properties) {
        this.restTemplate = builder.rootUri(properties.getBaseUri()).build();
    }

    @Override
    public Float convertProductPrice(Product product, String currency) {
        CurrencyApiResponseDTO currencyResponseDTO = this.restTemplate.getForObject("/" + currency + ".json",
                CurrencyApiResponseDTO.class);

        assert currencyResponseDTO != null;
        Float rate = currencyResponseDTO.getCurrencyRate().get(currency);

        return rate * product.getPrice();
    }
}
