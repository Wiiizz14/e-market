package com.example.spring.infrastructure;

import com.fasterxml.jackson.annotation.JsonAnySetter;
import lombok.Data;

import java.util.Collections;
import java.util.Map;

@Data
public class CurrencyApiResponseDTO {

    private String date;
    private Map<String, Float> currencyRate;

    @JsonAnySetter
    public void Add(String currency, Float rate) {
        this.currencyRate = Collections.singletonMap(currency, rate);
    }
}
