package com.example.spring.it;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;
import org.springframework.test.context.TestContext;
import org.springframework.test.context.TestExecutionListener;
import org.testcontainers.containers.PostgreSQLContainer;

import javax.sql.DataSource;
import java.util.Map;
import java.util.UUID;

/**
 * {@link TestExecutionListener} that start a new Postgres instance into a Docker container for integration tests.
 */

public class PostgresqlTestExecutionListener implements TestExecutionListener {

    // Postgres database attributes
    public static final String DB_NAME = "postgres_" + UUID.randomUUID();
    public static final String DB_USERNAME = "postgres";
    public static final String DB_PASSWORD = "password";
    /*Script to clean-up database before each test */
    public static final ResourceDatabasePopulator DB_CLEANUP_SCRIPT = new ResourceDatabasePopulator(new ClassPathResource("truncate.sql"));
    // Postgres container attributes
    private static final int POSTGRESQL_DEFAULT_PORT = 5432;
    private static final PostgreSQLContainer<?> postgresqlContainer;

    static {
        // Start a new Postgres container
        postgresqlContainer = new PostgreSQLContainer<>("postgres:13.4-alpine")
                .withDatabaseName(DB_NAME)
                .withUsername(DB_USERNAME)
                .withPassword(DB_PASSWORD)
                .withTmpFs(Map.of("/var/lib/postgres/data", ""));
        postgresqlContainer.start();

        // Override Spring properties to use the new Postgres instance
        System.setProperty("spring.datasource.url", postgresqlContainer.getJdbcUrl());
        System.setProperty("spring.datasource.username", DB_USERNAME);
        System.setProperty("spring.datasource.password", DB_PASSWORD);
    }


    private final Logger LOGGER = LoggerFactory.getLogger(PostgresqlTestExecutionListener.class);

    @Override
    public void beforeTestMethod(TestContext testContext) {
        LOGGER.info("Initialize database before test...");
        DataSource postgresDatasource = testContext.getApplicationContext().getBean(DataSource.class);
        DB_CLEANUP_SCRIPT.execute(postgresDatasource);
    }
}
