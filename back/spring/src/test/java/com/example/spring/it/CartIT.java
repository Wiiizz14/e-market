package com.example.spring.it;

import com.example.spring.controllers.dtos.CartDTO;
import com.example.spring.controllers.dtos.CartLineDTO;
import com.example.spring.controllers.dtos.ErrorDTO;
import com.example.spring.controllers.dtos.ProductDTO;
import com.example.spring.models.*;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.net.URI;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Objects;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.within;

@IntegrationTest
public class CartIT {

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private EntitiesPersister entitiesPersister;

    @Test
    public void shouldReturnNotfoundUnknownCart() {
        Integer unknownId = 100;
        ResponseEntity<ErrorDTO> response =
                restTemplate
                        .withBasicAuth("Alex", "alexb")
                        .getForEntity("/api/carts/{id}", ErrorDTO.class, unknownId);

        //When get cart
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
        assertThat(response.getBody()).isNotNull();
    }

    @Test
    public void shouldReturnExistingCart() {
        Address address = new Address("Rue de Paris", "1er étage", "35000", "Rennes", "FRANCE");
        User user = new User("Jean", "Dupont", "Jojo", address);
        Cart cart = new Cart(user.getId());

        entitiesPersister.persist(address, user, cart);

        //When get cart
        ResponseEntity<CartDTO> response =
                restTemplate
                        .withBasicAuth("Alex", "alexb")
                        .getForEntity("/api/carts/{id}", CartDTO.class, cart.getId());

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isNotNull()
                .returns(cart.getId(), CartDTO::getId)
                .returns(cart.getUserId(), CartDTO::getUserId);

        //Assert for TimeStamp with isCloseTo() for windows precision
        assertThat(response.getBody().getCreatedAt()).isCloseTo(cart.getCreatedAt(), within(1, ChronoUnit.MILLIS));
    }

    @Test
    public void shouldDeleteCart() {
        Address address = new Address("Rue de Paris", "1er étage", "35000", "Rennes", "FRANCE");
        User user = new User("Jean", "Dupont", "Jojo", address);
        entitiesPersister.persist(address, user);

        Cart cart = new Cart(user.getId());
        entitiesPersister.persist(cart);

        //When delete cart
        ResponseEntity<Void> deleteResponse =
                restTemplate
                        .withBasicAuth("Alex", "alexb")
                        .exchange(URI.create("/api/carts/" + cart.getId()), HttpMethod.DELETE, null, Void.class);

        assertThat(deleteResponse.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT);

        //When get cart
        ResponseEntity<ErrorDTO> getResponse =
                restTemplate
                        .withBasicAuth("Alex", "alexb")
                        .getForEntity("/api/carts/{id}", ErrorDTO.class, cart.getId());

        assertThat(getResponse.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    }

    @Test
    public void shouldCreateCart() {
        Address address = new Address("Rue de Paris", "1er étage", "35000", "Rennes", "FRANCE");
        User user = new User("Jean", "Dupont", "Jojo", address);
        entitiesPersister.persist(address, user);

        CartDTO inputDTO = new CartDTO();
        inputDTO.setUserId(user.getId());

        //When create cart
        ResponseEntity<CartDTO> response =
                restTemplate
                        .withBasicAuth("Alex", "alexb")
                        .postForEntity("/api/carts/", inputDTO, CartDTO.class);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.CREATED);
        assertThat(Objects.requireNonNull(response.getBody()).getId()).isNotNull();

        //When get cart
        ResponseEntity<CartDTO> getResponse =
                restTemplate
                        .withBasicAuth("Alex", "alexb")
                        .getForEntity(URI.create("/api/carts/" + response.getBody().getId()), CartDTO.class);

        assertThat(getResponse.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(getResponse.getBody())
                .returns(inputDTO.getUserId(), CartDTO::getUserId);

        //Assert for TimeStamp with isCloseTo() for windows precision
        assertThat(response.getBody().getCreatedAt()).isCloseTo(Instant.now(), within(3, ChronoUnit.SECONDS));
    }

    @Test
    public void shouldDeleteCarline() {
        Address address = new Address("Rue de Paris", "1er étage", "35000", "Rennes", "FRANCE");
        User user = new User("Jean", "Dupont", "Jojo", address);
        entitiesPersister.persist(address, user);

        Category category = new Category("CategoryTest");
        Product product = new Product("ProductTest", 123.99f, category);
        entitiesPersister.persist(category, product);

        Cart cart = new Cart(user.getId());
        CartLine cartLine = cart.addProduct(product, 456);
        entitiesPersister.persist(cart);

        //When delete cartline
        ResponseEntity<Void> deleteResponse =
                restTemplate
                        .withBasicAuth("Alex", "alexb")
                        .exchange(URI.create("/api/carts/" + cart.getId() + "/products/" + product.getId()), HttpMethod.DELETE, null, Void.class);

        assertThat(deleteResponse.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT);

        //When get cartline from cart
        ResponseEntity<CartDTO> getResponse =
                restTemplate
                        .withBasicAuth("Alex", "alexb")
                        .getForEntity(URI.create("/api/carts/" + cart.getId()), CartDTO.class);

        assertThat(getResponse.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(Objects.requireNonNull(getResponse.getBody()).getCartLines()).doesNotContain(CartLineDTO.fromDomain(cartLine));
    }

    @Test
    public void shouldCreateCartline() {
        Address address = new Address("Rue de Paris", "1er étage", "35000", "Rennes", "FRANCE");
        User user = new User("Jean", "Dupont", "Jojo", address);
        entitiesPersister.persist(address, user);

        Category category = new Category("CategoryTest");
        Product product = new Product("ProductTest", 123.99f, category);
        entitiesPersister.persist(category, product);

        Cart cart = new Cart(user.getId());
        entitiesPersister.persist(cart);

        CartLineDTO inputDTO = new CartLineDTO()
                .setProduct(ProductDTO.fromDomain(product))
                .setQuantity(456);

        //When create cartline
        ResponseEntity<CartLineDTO> response =
                restTemplate
                        .withBasicAuth("Alex", "alexb")
                        .postForEntity("/api/carts/" + cart.getId() + "/products/" + product.getId(), inputDTO, CartLineDTO.class);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.CREATED);
        assertThat(Objects.requireNonNull(response.getBody()).getProduct().getId()).isNotNull();

        //When get cartline from cart
        ResponseEntity<CartDTO> getResponse =
                restTemplate
                        .withBasicAuth("Alex", "alexb")
                        .getForEntity(URI.create("/api/carts/" + cart.getId()), CartDTO.class);

        assertThat(getResponse.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(Objects.requireNonNull(getResponse.getBody()).getCartLines()).contains(inputDTO);
    }
}
