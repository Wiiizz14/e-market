package com.example.spring.it;

import com.example.spring.Application;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestExecutionListeners;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;
import static org.springframework.test.context.TestExecutionListeners.MergeMode.MERGE_WITH_DEFAULTS;

/**
 * Annotation that must be specified on integration test classes.
 * Start the Spring Boot application with {@link org.springframework.context.annotation.Profile} <code>it</code> and Docker container for database.
 */
@SpringBootTest(webEnvironment = RANDOM_PORT, classes = Application.class)
@ActiveProfiles({"it"}) // Override default properties by values into `application-it.yml` file.
@TestExecutionListeners(
        mergeMode = MERGE_WITH_DEFAULTS,
        listeners = {PostgresqlTestExecutionListener.class})
@Retention(RUNTIME)
@Target(TYPE)

public @interface IntegrationTest {
}
