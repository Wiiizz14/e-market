package com.example.spring.it;


import com.example.spring.controllers.dtos.ErrorDTO;
import com.example.spring.controllers.dtos.ProductDTO;
import com.example.spring.models.Category;
import com.example.spring.models.Product;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.net.URI;
import java.util.Objects;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@IntegrationTest
public class ProductIT {

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private EntitiesPersister entitiesPersister;

    @Test
    public void shouldReturnNotFoundOnUnknownProduct() {
        Integer unknownId = 100;
        ResponseEntity<ErrorDTO> response =
                restTemplate
                        .withBasicAuth("Alex", "alexb")
                        .getForEntity("/api/products/{id}", ErrorDTO.class, unknownId);

        //When get Product
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
        assertThat(response.getBody()).isNotNull();
    }

    @Test
    public void shouldReturnExistingProduct() {
        Category category = new Category("CategoryTest");
        Product product = new Product("ProductTest", 123.99f, category);
        entitiesPersister.persist(category, product);

        //When get Product
        ResponseEntity<ProductDTO> response =
                restTemplate
                        .withBasicAuth("Alex", "alexb")
                        .getForEntity("/api/products/{id}", ProductDTO.class, product.getId());

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isNotNull()
                .returns(product.getId(), ProductDTO::getId)
                .returns(product.getName(), ProductDTO::getName)
                .returns(product.getPrice(), ProductDTO::getPrice)
                .returns(product.getCategory().getId(), ProductDTO::getCategoryId);
    }

    @Test
    public void shouldDeleteProduct() {
        Category category = new Category("CategoryTest");
        Product product = new Product("ProductTest", 123.99f, category);
        entitiesPersister.persist(category, product);

        //When delete product
        ResponseEntity<Void> deleteResponse =
                restTemplate
                        .withBasicAuth("Alex", "alexb")
                        .exchange(URI.create("/api/products/" + product.getId()), HttpMethod.DELETE, null, Void.class);

        assertThat(deleteResponse.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT);

        //When get product
        ResponseEntity<ErrorDTO> getResponse =
                restTemplate
                        .withBasicAuth("Alex", "alexb")
                        .getForEntity("/api/products/{id}", ErrorDTO.class, product.getId());

        assertThat(getResponse.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    }

    @Test
    public void shouldCreateProduct() {
        Category category = new Category("CategoryTest");
        entitiesPersister.persist(category);

        ProductDTO inputDTO = new ProductDTO();
        inputDTO.setName("ProductName");
        inputDTO.setPrice(12.99f);
        inputDTO.setCategoryId(category.getId());

        //When create product
        ResponseEntity<ProductDTO> response =
                restTemplate
                        .withBasicAuth("Alex", "alexb")
                        .postForEntity("/api/products", inputDTO, ProductDTO.class);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.CREATED);
        assertThat(Objects.requireNonNull(response.getBody()).getId()).isNotNull();

        //When get product
        ResponseEntity<ProductDTO> getResponse =
                restTemplate
                        .withBasicAuth("Alex", "alexb")
                        .getForEntity(URI.create("/api/products/" + response.getBody().getId()), ProductDTO.class);

        assertThat(getResponse.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(getResponse.getBody())
                .returns(inputDTO.getName(), ProductDTO::getName)
                .returns(inputDTO.getPrice(), ProductDTO::getPrice)
                .returns(inputDTO.getCategoryId(), ProductDTO::getCategoryId);
    }
}
