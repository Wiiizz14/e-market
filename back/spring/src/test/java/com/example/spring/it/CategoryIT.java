package com.example.spring.it;

import com.example.spring.controllers.dtos.CategoryDTO;
import com.example.spring.controllers.dtos.ErrorDTO;
import com.example.spring.models.Category;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.net.URI;
import java.util.Objects;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@IntegrationTest
public class CategoryIT {

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private EntitiesPersister entitiesPersister;

    @Test
    public void shouldReturnNotFoundUnknownCategory() {
        Integer unknownId = 100;
        ResponseEntity<ErrorDTO> response =
                restTemplate
                        .withBasicAuth("Alex", "alexb")
                        .getForEntity("/api/categories/{id}", ErrorDTO.class, unknownId);

        //When get category
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
        assertThat(response.getBody()).isNotNull();
    }

    @Test
    public void shouldReturnExistingCategory() {
        Category category = new Category("CategoryTest");
        entitiesPersister.persist(category);

        //When get category
        ResponseEntity<CategoryDTO> response =
                restTemplate
                        .withBasicAuth("Alex", "alexb")
                        .getForEntity("/api/categories/{id}", CategoryDTO.class, category.getId());

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isNotNull()
                .returns(category.getId(), CategoryDTO::getId)
                .returns(category.getName(), CategoryDTO::getName);
    }

    @Test
    public void shouldDeleteCategory() {
        Category category = new Category("CategoryTest");
        entitiesPersister.persist(category);

        //When delete category
        ResponseEntity<Void> deleteResponse =
                restTemplate
                        .withBasicAuth("Alex", "alexb")
                        .exchange(URI.create("/api/categories/" + category.getId()), HttpMethod.DELETE, null, Void.class);

        assertThat(deleteResponse.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT);
    }

    @Test
    public void shouldCreateCategory() {
        CategoryDTO inputDTO = new CategoryDTO();
        inputDTO.setName("CategoryTest");

        //When create category
        ResponseEntity<CategoryDTO> response =
                restTemplate
                        .withBasicAuth("Alex", "alexb")
                        .postForEntity("/api/categories", inputDTO, CategoryDTO.class);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.CREATED);
        assertThat(Objects.requireNonNull(response.getBody()).getId()).isNotNull();

        //When get category
        ResponseEntity<CategoryDTO> getResponse =
                restTemplate
                        .withBasicAuth("Alex", "alexb")
                        .getForEntity(URI.create("/api/categories/" + response.getBody().getId()), CategoryDTO.class);

        assertThat(getResponse.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(getResponse.getBody())
                .returns(inputDTO.getName(), CategoryDTO::getName);
    }
}