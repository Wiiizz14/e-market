package com.example.spring.models;

import com.example.spring.errors.BadRequestException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Set;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;


class CartTest {

    //Data test
    Category category = new Category("CategoryTest");
    Product productComputer = new Product("Computer", 49.95f, category);
    CartLine cartLineComputer = new CartLine(productComputer, 10);
    Product productServer = new Product("Server", 100.50f, category);
    CartLine cartLineServer = new CartLine(productServer, 20);
    Address address = new Address("street", "comp", "zip", "city", "country");
    User user = new User("firstname", "lastname", "pseudonym", address);

    @BeforeEach
    void setUp() {
        //Id set
        category.setId(1);
        productComputer.setId(1);
        productServer.setId(2);
        address.setId(1);
        user.setId(1);
    }

    @Test
    void get_cartLine_by_product_id() {
        //Init
        Cart cart = new Cart(user.getId());
        cart.getCartLines().add(cartLineComputer);

        //Expected
        CartLine cartLineExpected = cart.getCartLineByProductId(productComputer.getId());

        //Test
        assertEquals(cartLineComputer, cartLineExpected);
    }

    @Test
    void get_cartLine_by_product() {
        //Init
        Cart cart = new Cart(user.getId());
        cart.getCartLines().add(cartLineComputer);

        //Expected
        CartLine cartLineExpected = cart.getCartLineByProduct(productComputer);

        //Test
        assertEquals(cartLineComputer, cartLineExpected);
    }

    @Test
    void add_product_with_quantity_negative() {
        //Init
        Cart cart = new Cart(user.getId());
        cart.getCartLines().add(cartLineComputer);

        //Test
        assertThatThrownBy(() -> cart.addProduct(productComputer, 0))
                .isInstanceOf(BadRequestException.class);
    }

    @Test
    void add_existing_Product() {
        //Init
        Cart cart = new Cart(user.getId());
        cart.addProduct(productComputer, 1);
        cart.addProduct(productComputer, 3);

        //Expected
        int sizeExpected = cart.getCartLines().size();
        int productQuantityExpected = cart.getCartLineByProduct(productComputer).getQuantity();
        float priceExpected = cart.getTotalPrice();

        //Test
        assertEquals(1, sizeExpected);
        assertEquals(4, productQuantityExpected);
        assertEquals(199.8f, priceExpected);
    }

    @Test
    void add_non_existing_product() {
        //Init
        Cart cart = new Cart(user.getId());

        //Expected
        int productQuantityExpected = cart.addProduct(productComputer, 1).getQuantity();
        int productIdExpected = cart.addProduct(productComputer, 1).getProduct().getId();
        int sizeExpected = cart.getCartLines().size();

        //Test
        assertEquals(1, productQuantityExpected);
        assertEquals(productComputer.getId(), productIdExpected);
        assertEquals(1, sizeExpected);
    }

    @Test
    void delete_a_cartLine_when_cart_contains_only_one() {
        //Init
        Cart cart = new Cart(user.getId());
        cart.getCartLines().add(cartLineComputer);
        cart.deleteCartLine(cartLineComputer);

        //Expected
        int sizeExpected = 0;
        CartLine cartLineExpected = cart.getCartLineByProductId(productComputer.getId());
        float priceExpected = cart.getTotalPrice();

        //Test
        assertEquals(0, sizeExpected);
        assertNull(cartLineExpected);
        assertEquals(0, priceExpected);
    }

    @Test
    void delete_a_cartLine_when_cart_contains_many() {
        //Init
        Cart cart = new Cart(user.getId());
        cart.getCartLines().add(cartLineComputer);
        cart.getCartLines().add(cartLineServer);
        cart.deleteCartLine(cartLineComputer);

        //Expected
        int sizeExpected = 1;
        CartLine cartLineDeleteExpected = cart.getCartLineByProductId(productComputer.getId());
        CartLine cartlineExistingExpected = cart.getCartLineByProductId(productServer.getId());
        float priceExpected = cart.getTotalPrice();

        //Test
        assertEquals(1, sizeExpected);
        assertNull(cartLineDeleteExpected);
        assertEquals(cartLineServer, cartlineExistingExpected);
        assertEquals(2010.0f, priceExpected);
    }

    @Test
    void getTotalPrice() {
        //Init
        Cart cart = new Cart(user.getId());
        Set<CartLine> cartlines = cart.getCartLines();
        cartlines.add(cartLineComputer);

        //Expected
        float priceExpected = cart.getTotalPrice();

        //Test
        assertEquals(499.5f, priceExpected);
    }
}