package com.example.spring.it;

import com.example.spring.controllers.dtos.AddressDTO;
import com.example.spring.controllers.dtos.ErrorDTO;
import com.example.spring.controllers.dtos.UserDTO;
import com.example.spring.models.Address;
import com.example.spring.models.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.net.URI;
import java.util.Objects;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@IntegrationTest
public class UserIT {

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private EntitiesPersister entitiesPersister;

    @Test
    public void shouldReturnNotFoundUnknownUser() {
        Integer unknownId = 100;
        ResponseEntity<ErrorDTO> response =
                restTemplate
                        .withBasicAuth("Alex", "alexb")
                        .getForEntity("/api/users/{id}", ErrorDTO.class, unknownId);

        //When get user
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
        assertThat(response.getBody()).isNotNull();
    }

    @Test
    public void shouldReturnExistingUser() {
        Address address = new Address("Rue de Paris", "1er étage", "35000", "Rennes", "FRANCE");
        User user = new User("Jean", "Dupont", "Jojo", address);
        entitiesPersister.persist(address, user);

        //When get user
        ResponseEntity<UserDTO> response =
                restTemplate
                        .withBasicAuth("Alex", "alexb")
                        .getForEntity("/api/users/{id}", UserDTO.class, user.getId());

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isNotNull()
                .returns(user.getId(), UserDTO::getId)
                .returns(user.getFirstname(), UserDTO::getFirstname)
                .returns(user.getLastname(), UserDTO::getLastname)
                .returns(user.getPseudonym(), UserDTO::getPseudonym)
                .returns(user.getAddress().getId(), dto -> dto.getAddress().getId());
    }

    @Test
    public void shouldDeleteUser() {
        Address address = new Address("Rue de Paris", "1er étage", "35000", "Rennes", "FRANCE");
        User user = new User("Jean", "Dupont", "Jojo", address);
        entitiesPersister.persist(address, user);

        //When delete user
        ResponseEntity<Void> deleteResponse =
                restTemplate
                        .withBasicAuth("Alex", "alexb")
                        .exchange(URI.create("/api/users/" + user.getId()), HttpMethod.DELETE, null, Void.class);

        assertThat(deleteResponse.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT);

        //When get user
        ResponseEntity<ErrorDTO> getResponse =
                restTemplate
                        .withBasicAuth("Alex", "alexb")
                        .getForEntity("/api/users/{id}", ErrorDTO.class, user.getId());

        assertThat(getResponse.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    }

    @Test
    public void shouldCreateUser() {
        UserDTO inputDTO = new UserDTO();
        inputDTO.setFirstname("Jean");
        inputDTO.setLastname("Dupont");
        inputDTO.setPseudonym("Jojo");

        AddressDTO addressDTO = new AddressDTO()
                .setStreet("Rue de Paris")
                .setComplement("1er étage")
                .setZip("35000")
                .setCity("Rennes")
                .setCountry("FRANCE");
        inputDTO.setAddress(addressDTO);

        //When create user
        ResponseEntity<UserDTO> response =
                restTemplate
                        .withBasicAuth("Alex", "alexb")
                        .postForEntity("/api/users/", inputDTO, UserDTO.class);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.CREATED);
        assertThat(Objects.requireNonNull(response.getBody()).getId()).isNotNull();

        //When get user
        ResponseEntity<UserDTO> getResponse =
                restTemplate
                        .withBasicAuth("Alex", "alexb")
                        .getForEntity(URI.create("/api/users/" + response.getBody().getId()), UserDTO.class);

        assertThat(getResponse.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(getResponse.getBody())
                .returns(inputDTO.getFirstname(), UserDTO::getFirstname)
                .returns(inputDTO.getLastname(), UserDTO::getLastname)
                .returns(inputDTO.getPseudonym(), UserDTO::getPseudonym)
                .returns(inputDTO.getAddress().getStreet(), dto -> dto.getAddress().getStreet());

        assertThat(Objects.requireNonNull(getResponse.getBody()).getAddress().getId()).isNotNull();
    }
}
