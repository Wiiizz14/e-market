#!/bin/bash

mvn clean verify
docker build -t registry.gitlab.com/zenika-poei-rennes-04/tp-e-market-web-tj/back/spring:latest  .
