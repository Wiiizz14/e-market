<h1 align="center">Welcome to TP-eMarket 👋</h1>
<p>
  <img alt="Version" src="https://img.shields.io/badge/version-1.0-blue.svg?cacheSeconds=2592000" />
</p>

> TP fil rouge réalisé durant la session de formation Zenika

## Build

Linux users : 
```sh
./build.sh
```

Windows users :
```ps1
./build.ps1
```

## Start
```sh
docker-compose up
```

## Author

👤 **Thibault James**


## Show your support

Give a ⭐️ if this project helped you!
