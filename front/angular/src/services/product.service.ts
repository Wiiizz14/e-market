import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Product} from "../interfaces/Product";
import {Page} from "../interfaces/Page";
import {environment} from "../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  url: string = environment.apiUrl;

  constructor(private httpRequest: HttpClient) {
    this.httpRequest = httpRequest;
  }

  createProduct(product: Product) {
    return this.httpRequest.post<Product>(this.url + "/api/products", product);
  }

  getProductPage(page: number) {
    return this.httpRequest.get<Page<Product>>(this.url + "/api/products?page=" + page);
  }

  getProductList() {
    return this.httpRequest.get<Page<Product>>(this.url + "/api/products");
  }

  getProductById(id: number) {
    return this.httpRequest.get<Product>(this.url + "/api/products/" + id);
  }

  updateProduct(product: Product) {
    return this.httpRequest.put<Product>(this.url + "/api/products/" + product.id, product);
  }

  deleteProduct(id: number) {
    return this.httpRequest.delete<Product>(this.url + "/api/products/" + id);
  }
}
