import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Cart} from "../interfaces/Cart";
import {environment} from "../environments/environment";
import {CartLines} from "../interfaces/CartLines";

@Injectable({
  providedIn: 'root'
})
export class CartService {

  url: string = environment.apiUrl;

  constructor(private httpRequest: HttpClient) {
    this.httpRequest = httpRequest
  }

  createCart(userId: number) {
    return this.httpRequest.post<Cart>(this.url + "/api/carts", {userId: userId});
  }

  getCarts() {
    return this.httpRequest.get<Cart[]>(this.url + "/api/carts");
  }

  getCartById(id: number) {
    return this.httpRequest.get<Cart>(this.url + "/api/carts/" + id);
  }

  deleteCart(id: number) {
    return this.httpRequest.delete<Cart>(this.url + "/api/carts/" + id);
  }

  addProductToCart(cartId: number, productId: number, quantity: number) {
    return this.httpRequest.post<CartLines>(this.url + "/api/carts/" + cartId + "/products/" + productId, {quantity : quantity});
  }

  deleteProductOnCart(cartId: number, productId: number){
    return this.httpRequest.delete(this.url + "/api/carts/" + cartId + "/products/" + productId);
  }
}
