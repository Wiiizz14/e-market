import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Category} from "../interfaces/Category";
import {environment} from "../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  url: string = environment.apiUrl;

  constructor(private httpRequest: HttpClient) {
    this.httpRequest = httpRequest
  }

  createCategory(category: Category) {
    return this.httpRequest.post<Category>(this.url + "/api/categories", category);
  }

  getCategories() {
    return this.httpRequest.get<Category[]>(this.url + "/api/categories");
  }

  getCategoryById(id: number) {
    return this.httpRequest.get<Category>(this.url + "/api/categories/" + id);
  }

  updateCategory(category: Category) {
    return this.httpRequest.put<Category>(this.url + "/api/categories/" + category.id, category);
  }

  deleteCategory(id: number) {
    return this.httpRequest.delete<Category>(this.url + "/api/categories/" + id);
  }
}
