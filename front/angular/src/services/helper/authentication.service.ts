import {Injectable} from '@angular/core';
import {Login} from "../../interfaces/Login";
import {User} from "../../interfaces/User";
import {HttpClient} from "@angular/common/http";
import {environment} from "../../environments/environment";
import {CartService} from "../cart.service";
import {Router} from "@angular/router";


@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  url: string = environment.apiUrl;
  private readonly SESSION_STORAGE_KEY = "currentUser";

  constructor(
    private httpRequest: HttpClient,
    private routeRedir: Router,
    private cartService: CartService) {
    this.httpRequest = httpRequest;
  }

  login(login: Login) {
    sessionStorage.setItem(this.SESSION_STORAGE_KEY, JSON.stringify(login));

    //Create cart
    this.getCurrentUser().subscribe((user) => {
      this.cartService.createCart(user.id).subscribe(
        data => this.routeRedir.navigate(['']).then(() => location.reload()),
        error => this.routeRedir.navigate(['carts/' + user.id]).then(() => location.reload())
      );
    });
  }

  logout(): void {
    sessionStorage.removeItem(this.SESSION_STORAGE_KEY);
  }

  getCurrentUserBasicAuthentication(): string | undefined {
    const currentUserPlain = sessionStorage.getItem(this.SESSION_STORAGE_KEY);
    if (currentUserPlain) {
      const currentUser: User = JSON.parse(currentUserPlain);
      return "Basic " + btoa(currentUser.pseudonym + ":" + currentUser.password);
    } else {
      return undefined;
    }
  }

  getCurrentUser() {
    return this.httpRequest.get<User>(this.url + "/api/profiles/me");
  }

  getGlobalAuth(): [boolean, boolean] {
    let auth = this.getCurrentUserBasicAuthentication();

    let isAuth = false;
    let isAdm = false;

    if (auth != undefined) {
      isAuth = true;
    }

    if (auth === "Basic QWxleDphbGV4Yg==") {
      isAdm = true;
    }
    return [isAuth, isAdm];
  }
}
