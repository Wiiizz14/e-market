import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {User} from "../interfaces/User";
import {Page} from "../interfaces/Page";
import {environment} from "../environments/environment";
import {AuthenticationService} from "./helper/authentication.service";
import {Cart} from "../interfaces/Cart";

@Injectable({
  providedIn: 'root'
})
export class UserService {

  url: string = environment.apiUrl;
  id!: number;

  constructor(private httpRequest: HttpClient, private authService: AuthenticationService) {
    this.httpRequest = httpRequest;
    this.authService = authService;
  }

  getUserPage(page: number) {
    return this.httpRequest.get<Page<User>>(this.url + "/api/users?page=" + page);
  }

  getUserList() {
    return this.httpRequest.get<Page<User>>(this.url + "/api/users");
  }

  getUserById(id: number) {
    return this.httpRequest.get<User>(this.url + "/api/users/" + id);
  }

  getCartByUserId(userId: number){
    return this.httpRequest.get<Cart>(this.url + "/api/users/" + userId + "/cart");
  }
}
