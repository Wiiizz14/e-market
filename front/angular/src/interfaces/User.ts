import {Address} from "./Address";

export interface User{
  id: number
  firstname: string
  lastname : string
  pseudonym: string
  password: string
  address: Address
}
