import {Product} from "./Product";

export interface CartLines{

  product: Product
  quantity: number
  price: number
}
