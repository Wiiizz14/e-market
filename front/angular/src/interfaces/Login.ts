export interface Login {

  pseudonym: string,
  password: string
}
