import {CartLines} from "./CartLines";

export interface Cart {

  id: number
  userId: number
  createdAt: string
  cartLines: CartLines[]
  price: number;
}
