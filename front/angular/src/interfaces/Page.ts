export interface Page<T> {

  content: Array<T>
  page: number
  size: number
  count: number
  totalCount: number
  totalPage: number
}
