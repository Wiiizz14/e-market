export interface Address {

  id: number
  street: string
  complement: string
  zip: string
  city: string
  country: string
}
