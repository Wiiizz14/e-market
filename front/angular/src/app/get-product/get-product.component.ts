import {Component, OnInit} from '@angular/core';
import {Product} from "../../interfaces/Product";
import {ActivatedRoute, Router} from "@angular/router";
import {ProductService} from "../../services/product.service";
import {CategoryService} from "../../services/category.service";
import {Category} from "../../interfaces/Category";
import {AuthenticationService} from "../../services/helper/authentication.service";
import {CartService} from "../../services/cart.service";
import {UserService} from "../../services/user.service";
import {User} from "../../interfaces/User";

@Component({
  selector: 'app-get-product',
  templateUrl: './get-product.component.html',
  styleUrls: ['./get-product.component.css']
})
export class GetProductComponent implements OnInit {

  product!: Product;
  category!: Category;
  quantity: number = 1;

  user!: User;

  isAuthenticated: boolean = false;
  isAdmin: boolean = false;

  constructor(
    private route: ActivatedRoute,
    private routeRedir: Router,
    private authenticationService: AuthenticationService,
    private productService: ProductService,
    private categoryService: CategoryService,
    private cartService: CartService,
    private userService: UserService) {
  }

  ngOnInit(): void {
    const routeParams = this.route.snapshot.paramMap;
    const productIdFromRoute = Number(routeParams.get("id"));

    this.productService.getProductById(productIdFromRoute).subscribe((product) => {
      this.product = product;
      this.categoryService.getCategoryById(this.product.categoryId).subscribe((category) => this.category = category);
    });

    //Get auth & role
    [this.isAuthenticated, this.isAdmin] = this.authenticationService.getGlobalAuth();
  }

  onProductUpdateClick(id: number) {
    this.routeRedir.navigate(['products/' + id + "/update"]);
  }

  onProductDeleteClick(id: number) {
    if (this.isAdmin) {
      this.productService.deleteProduct(id).subscribe();
      window.alert('Product deleted !');
      this.routeRedir.navigate(['products']).then(() => location.reload());
    } else {
      window.alert("You are not administrator !");
      this.routeRedir.navigate(['']);
    }
  }

  onAddToCart(product: Product) {
    this.authenticationService.getCurrentUser().subscribe((user) => {
      this.userService.getCartByUserId(user.id).subscribe((cart) => {
        this.cartService.addProductToCart(cart.id, product.id, this.quantity).subscribe(() => {
            this.routeRedir.navigate(['/carts/' + user.id]);
          }
        );
      })
    });
  }

}
