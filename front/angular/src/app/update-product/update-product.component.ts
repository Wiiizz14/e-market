import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {ProductService} from "../../services/product.service";
import {Product} from "../../interfaces/Product";
import {AuthenticationService} from "../../services/helper/authentication.service";

@Component({
  selector: 'app-update-product',
  templateUrl: './update-product.component.html',
  styleUrls: ['./update-product.component.css']
})
export class UpdateProductComponent implements OnInit {

  id!: number;

  isAuthenticated: boolean = false;
  isAdmin: boolean = false;

  constructor(
    private route: ActivatedRoute,
    private routeRedir: Router,
    private authenticationService: AuthenticationService,
    private productService: ProductService) {
  }

  ngOnInit(): void {
    this.id = this.route.snapshot.params['id'];

    //Get status
    [this.isAuthenticated, this.isAdmin] = this.authenticationService.getGlobalAuth();

    if (!this.isAdmin) {
      window.alert("You are not administrator !");
      this.routeRedir.navigate(['']);
    }
  }

  onSubmit(productToUpdate: Product) {
    productToUpdate.id = this.id;
    this.productService.updateProduct(productToUpdate).subscribe();
  }
}
