import {NgModule} from '@angular/core';
import {RouterModule} from "@angular/router";
import {BrowserModule} from '@angular/platform-browser';
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";

import {AppComponent} from './app.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {TokenInterceptorService} from "../services/helper/token-interceptor.service";
import {LoginFormComponent} from './login-form/login-form.component';
import {NavbarComponent} from './navbar/navbar.component';
import {GetHomepageComponent} from './get-homepage/get-homepage.component';
import {GetProductListComponent} from "./get-product-list/get-product-list.component";
import {GetProductComponent} from './get-product/get-product.component';
import {ProductFormComponent} from './product-form/product-form.component';
import {CreateProductComponent} from './create-product/create-product.component';
import {UpdateProductComponent} from './update-product/update-product.component';
import {GetCategoryListComponent} from './get-category-list/get-category-list.component';
import {GetCategoryComponent} from './get-category/get-category.component';
import {CategoryFormComponent} from './category-form/category-form.component';
import {CreateCategoryComponent} from './create-category/create-category.component';
import {UpdateCategoryComponent} from './update-category/update-category.component';
import {GetCartListComponent} from './get-cart-list/get-cart-list.component';
import {GetCartComponent} from './get-cart/get-cart.component';
import {GetUserListComponent} from './get-user-list/get-user-list.component';
import {GetUserComponent} from './get-user/get-user.component';
import {ErrorInterceptor} from "../services/helper/ErrorInterceptor";

@NgModule({
  declarations: [
    AppComponent,
    LoginFormComponent,
    NavbarComponent,
    GetHomepageComponent,
    GetProductListComponent,
    GetProductComponent,
    ProductFormComponent,
    CreateProductComponent,
    UpdateProductComponent,
    GetCategoryListComponent,
    GetCategoryComponent,
    CategoryFormComponent,
    CreateCategoryComponent,
    UpdateCategoryComponent,
    GetCartListComponent,
    GetCartComponent,
    GetUserListComponent,
    GetUserComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot([
      {path: '', component: GetHomepageComponent},
      {path: 'login', component: LoginFormComponent},
      {path: 'products', component: GetProductListComponent},
      {path: 'products/new', component: CreateProductComponent},
      {path: 'products/:id/update', component: UpdateProductComponent},
      {path: 'products/:id', component: GetProductComponent},
      {path: 'categories', component: GetCategoryListComponent},
      {path: 'categories/new', component: CreateCategoryComponent},
      {path: 'categories/:id/update', component: UpdateCategoryComponent},
      {path: 'categories/:id', component: GetCategoryComponent},
      {path: 'carts', component: GetCartListComponent},
      {path: 'carts/:id', component: GetCartComponent},
      {path: 'users', component: GetUserListComponent},
      {path: 'users/:id', component: GetUserComponent},
    ]),
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [{ provide: HTTP_INTERCEPTORS, useClass: TokenInterceptorService, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true }],

  bootstrap: [AppComponent]
})
export class AppModule {
}
