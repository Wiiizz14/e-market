import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Login} from "../../interfaces/Login";
import {AuthenticationService} from "../../services/helper/authentication.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})
export class LoginFormComponent implements OnInit {
  @Output() eventForm = new EventEmitter();
  checkoutForm!: FormGroup;
  submitted = false;

  constructor(
    private formBuilder: FormBuilder,
    private authenticationService: AuthenticationService) {
  }

  get field() { return this.checkoutForm.controls; }

  ngOnInit(): void {
    this.checkoutForm = this.formBuilder.group({
      pseudonym: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  onSubmit() {
    let login!: Login;
    login = this.checkoutForm.value;

    this.submitted = true;

    if (this.checkoutForm.valid) {
      this.authenticationService.login(login);
    }
  }
}
