import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {CategoryService} from "../../services/category.service";
import {ActivatedRoute, Router} from "@angular/router";
import {Category} from "../../interfaces/Category";

@Component({
  selector: 'app-category-form',
  templateUrl: './category-form.component.html',
  styleUrls: ['./category-form.component.css']
})
export class CategoryFormComponent implements OnInit {

  @Input() mode!: 'new' | 'update';
  @Output() eventForm = new EventEmitter();

  categoryForm!: FormGroup;
  id!: number;
  submitted = false;

  constructor(
    private formBuilder: FormBuilder,
    private categoryService: CategoryService,
    private route: ActivatedRoute,
    private routeRedir: Router
  ) {
  }

  get field() {
    return this.categoryForm.controls;
  }

  ngOnInit(): void {
    this.categoryForm = this.formBuilder.group({
      name: ['', Validators.required]
    });

    if (this.mode === 'update') {
      this.id = this.route.snapshot.params['id'];

      this.categoryService
        .getCategoryById(this.id)
        .subscribe((x) => this.categoryForm.patchValue(x));
    }
  }

  onSubmit() {
    this.submitted = true;
    let category: Category = this.categoryForm.value;

    if (this.categoryForm.valid) {
      category.id = this.id;
      this.eventForm.emit(category);
      this.categoryForm.reset();

      if (this.mode === 'new') {
        window.alert('Category created !');
      } else {
        window.alert('Category updated');
      }

      //Redirect to homepage and refresh to display changes
      this.routeRedir.navigate(['']).then(() => location.reload());

    } else {
      window.alert('Form is invalid');
      return;
    }
  }

}

