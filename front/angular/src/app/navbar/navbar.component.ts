import {Component, OnInit} from '@angular/core';
import {environment} from "../../environments/environment";
import {Observable} from "rxjs";
import {Category} from "../../interfaces/Category";
import {map} from "rxjs/operators";
import {CategoryService} from "../../services/category.service";
import {ActivatedRoute, Router} from "@angular/router";
import {AuthenticationService} from "../../services/helper/authentication.service";

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  url: string = environment.apiUrl;
  categories!: Observable<Category[]>;
  category!: Observable<Category>;

  isAuthenticated: boolean = false;
  isAdmin: boolean = false;

  constructor(
    private route: ActivatedRoute,
    private routeRedir: Router,
    private authenticationService: AuthenticationService,
    private categoryService: CategoryService) {
  }

  private static warnLoggedOut() {
    window.alert("You are deconnected !");
  }

  ngOnInit(): void {
    //Get category list and sort it by id value
    this.categories = this.categoryService.getCategories().pipe(map((items) => {
      items.sort((a, b) => {
        return a.id < b.id ? -1 : 1;
      });
      return items;
    }));

    //Get auth & role
    [this.isAuthenticated, this.isAdmin] = this.authenticationService.getGlobalAuth();
    console.log("Auth ? : " + this.isAuthenticated);
    console.log("Adm ? : " + this.isAdmin);
  }

  onHomepageClick() {
    this.routeRedir.navigate(['']);
  }

  onProductClick() {
    this.routeRedir.navigate(['products']);
  }

  onCreateProductClick() {
    this.routeRedir.navigate(['products/new']);
  }

  onCategoriesClick() {
    this.routeRedir.navigate(['categories']);
  }

  onCategoryClick(id: number) {
    this.routeRedir.navigate(['categories/' + id]);
  }

  onCreateCategoryClick() {
    this.routeRedir.navigate(['categories/new']);
  }

  onCartClick() {
    this.authenticationService.getCurrentUser().subscribe((user) => {
      this.routeRedir.navigate(['carts/' + user.id]);
    });
  }

  onCartsClick() {
    this.routeRedir.navigate(['carts']);
  }

  onUserClick() {
    this.authenticationService.getCurrentUser().subscribe((user) => {
      this.routeRedir.navigate(['users/' + user.id]);
    });
  }

  onUsersClick() {
    this.routeRedir.navigate(['users']);
  }

  onLoginClick() {
    this.routeRedir.navigate(['login']);
  }

  onLogoutClick() {
    this.authenticationService.logout();
    NavbarComponent.warnLoggedOut();
    this.routeRedir.navigate(['']).then(() => location.reload());
  }
}
