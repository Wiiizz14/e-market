import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ProductService} from "../../services/product.service";
import {ActivatedRoute, Router} from "@angular/router";
import {Product} from "../../interfaces/Product";
import {map} from "rxjs/operators";
import {CategoryService} from "../../services/category.service";
import {Observable} from "rxjs";
import {Category} from "../../interfaces/Category";

@Component({
  selector: 'app-product-form',
  templateUrl: './product-form.component.html',
  styleUrls: ['./product-form.component.css']
})
export class ProductFormComponent implements OnInit {

  @Input() mode!: 'new' | 'update';
  @Output() eventForm = new EventEmitter();

  categories!: Observable<Category[]>;

  productForm!: FormGroup;
  id!: number;
  submitted = false;

  constructor(
    private formBuilder: FormBuilder,
    private productService: ProductService,
    private categoryService: CategoryService,
    private route: ActivatedRoute,
    private routeRedir: Router
  ) {
  }

  get field() {
    return this.productForm.controls;
  }

  ngOnInit(): void {
    this.productForm = this.formBuilder.group({
      name: ['', Validators.required],
      price: ['', Validators.required],
      categoryId: ['', Validators.required]
    });

    //Get category list for selection
    this.categories = this.categoryService.getCategories().pipe(map((items) => {
      items.sort((a, b) => {
        return a.id < b.id ? -1 : 1;
      });
      return items;
    }));

    if (this.mode === 'update') {
      this.id = this.route.snapshot.params['id'];

      this.productService
        .getProductById(this.id)
        .subscribe((x) => this.productForm.patchValue(x));
    }
  }

  onSubmit() {
    this.submitted = true;
    let product: Product = this.productForm.value;

    if (this.productForm.valid) {
      product.id = this.id;
      this.eventForm.emit(product);
      this.productForm.reset();

      if (this.mode === 'new') {
        window.alert('Product created !');
      } else {
        window.alert('Product updated');
      }

      //Redirect to homepage and refresh to display changes
      this.routeRedir.navigate(['']).then(() => location.reload());

    } else {
      window.alert('Form is invalid');
      return;
    }
  }
}
