import {Component, OnInit} from '@angular/core';
import {CategoryService} from "../../services/category.service";
import {Category} from "../../interfaces/Category";
import {ActivatedRoute, Router} from "@angular/router";
import {AuthenticationService} from "../../services/helper/authentication.service";

@Component({
  selector: 'app-update-category',
  templateUrl: './update-category.component.html',
  styleUrls: ['./update-category.component.css']
})
export class UpdateCategoryComponent implements OnInit {

  id!: number;

  isAuthenticated: boolean = false;
  isAdmin: boolean = false;

  constructor(
    private route: ActivatedRoute,
    private routeRedir: Router,
    private authenticationService: AuthenticationService,
    private categoryService: CategoryService) {
  }

  ngOnInit(): void {
    this.id = this.route.snapshot.params['id'];

    //Get status
    [this.isAuthenticated, this.isAdmin] = this.authenticationService.getGlobalAuth();

    if (!this.isAdmin) {
      window.alert("You are not administrator !");
      this.routeRedir.navigate(['']);
    }
  }

  onSubmit(categoryToUpdate: Category) {
    categoryToUpdate.id = this.id;
    this.categoryService.updateCategory(categoryToUpdate).subscribe();
  }
}
