import {Component, OnInit} from '@angular/core';
import {Observable} from "rxjs";
import {Cart} from "../../interfaces/Cart";
import {ActivatedRoute, Router} from "@angular/router";
import {CartService} from "../../services/cart.service";
import {map} from "rxjs/operators";
import {AuthenticationService} from "../../services/helper/authentication.service";
import {UserService} from "../../services/user.service";

@Component({
  selector: 'app-get-cart',
  templateUrl: './get-cart.component.html',
  styleUrls: ['./get-cart.component.css']
})
export class GetCartComponent implements OnInit {

  cart!: Observable<Cart>;

  constructor(
    private route: ActivatedRoute,
    private routeRedir: Router,
    private authenticationService: AuthenticationService,
    private userService: UserService,
    private cartService: CartService) {
  }

  ngOnInit(): void {
    this.route.params.subscribe(routeParam => {
      this.cart = this.userService.getCartByUserId(routeParam.id).pipe(map((cart) => {
        cart.cartLines.sort((a, b) => {
          return a.product.id < b.product.id ? -1 : 1;
        });
        return cart;
      }));
    });
  }

  onUpdateQuantityProduct(productId: number, qty: string) {
    //Remove product before update new quantity
    this.onRemoveProduct(productId);

    //Transform quantity string to number
    let quantity = Number(qty);

    //Update cartline
    this.authenticationService.getCurrentUser().subscribe((user) => {
      this.userService.getCartByUserId(user.id).subscribe((cart) => {
        this.cartService.addProductToCart(cart.id, productId, quantity).subscribe(() => {
            this.routeRedir.navigate(['/carts/' + user.id]).then(() => location.reload());
          }
        );
      });
    });
  }

  onRemoveProduct(productId: number) {
    this.authenticationService.getCurrentUser().subscribe((user) => {
      this.userService.getCartByUserId(user.id).subscribe((cart) => {
        this.cartService.deleteProductOnCart(cart.id, productId).subscribe(() => {
            this.routeRedir.navigate(['/carts/' + user.id]).then(() => location.reload());
          }
        );
      });
    });
  }
}
