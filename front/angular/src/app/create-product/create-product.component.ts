import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {ProductService} from "../../services/product.service";
import {Product} from "../../interfaces/Product";
import {AuthenticationService} from "../../services/helper/authentication.service";

@Component({
  selector: 'app-create-product',
  templateUrl: './create-product.component.html',
  styleUrls: ['./create-product.component.css']
})
export class CreateProductComponent implements OnInit {

  isAuthenticated: boolean = false;
  isAdmin: boolean = false;

  constructor(
    private route: Router,
    private authenticationService: AuthenticationService,
    private productService: ProductService) {
  }

  ngOnInit(): void {
    //Get auth & role
    [this.isAuthenticated, this.isAdmin] = this.authenticationService.getGlobalAuth();

    if (!this.isAdmin) {
      window.alert("You are not administrator !");
      this.route.navigate(['']);
    }
  }

  onSubmit(productToCreate: Product) {
    this.productService.createProduct(productToCreate).subscribe();
  }
}
