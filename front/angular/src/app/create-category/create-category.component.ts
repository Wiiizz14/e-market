import {Component, OnInit} from '@angular/core';
import {CategoryService} from "../../services/category.service";
import {Category} from "../../interfaces/Category";
import {Router} from "@angular/router";
import {AuthenticationService} from "../../services/helper/authentication.service";

@Component({
  selector: 'app-create-category',
  templateUrl: './create-category.component.html',
  styleUrls: ['./create-category.component.css']
})
export class CreateCategoryComponent implements OnInit {

  isAuthenticated: boolean = false;
  isAdmin: boolean = false;

  constructor(
    private route: Router,
    private authenticationService: AuthenticationService,
    private categoryService: CategoryService) {
  }

  ngOnInit(): void {
    //Get auth & role
    [this.isAuthenticated, this.isAdmin] = this.authenticationService.getGlobalAuth();

    if (!this.isAdmin) {
      window.alert("You are not administrator !");
      this.route.navigate(['']);
    }
  }

  onSubmit(categoryToCreate: Category) {
    this.categoryService.createCategory(categoryToCreate).subscribe();
  }
}
