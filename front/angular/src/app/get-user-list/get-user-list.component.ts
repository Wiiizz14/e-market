import {Component, OnInit} from '@angular/core';
import {UserService} from "../../services/user.service";
import {Observable} from "rxjs";
import {User} from "../../interfaces/User";
import {Page} from "../../interfaces/Page";
import {map} from "rxjs/operators";

@Component({
  selector: 'app-get-user-list',
  templateUrl: './get-user-list.component.html',
  styleUrls: ['./get-user-list.component.css']
})
export class GetUserListComponent implements OnInit {

  users!: Observable<User[]>;
  page!: Observable<Page<User>>;

  constructor(private userService: UserService) {
  }

  displayPageContent(pageToDisplay: number) {
    //Get new page to display
    this.page = this.userService.getUserPage(pageToDisplay);

    //Set obs list
    this.users = this.page.pipe(map(page => page.content));
  }

  ngOnInit(): void {
    //Get obs page
    this.page = this.userService.getUserList();

    //Set obs list
    this.users = this.page.pipe(map(page => page.content));
  }
}
