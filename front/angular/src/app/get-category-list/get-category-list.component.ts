import {Component, OnInit} from '@angular/core';
import {Observable} from "rxjs";
import {Category} from "../../interfaces/Category";
import {CategoryService} from "../../services/category.service";
import {map} from "rxjs/operators";
import {Router} from "@angular/router";
import {AuthenticationService} from "../../services/helper/authentication.service";

@Component({
  selector: 'app-get-category-list',
  templateUrl: './get-category-list.component.html',
  styleUrls: ['./get-category-list.component.css']
})
export class GetCategoryListComponent implements OnInit {

  categories!: Observable<Category[]>;

  isAuthenticated: boolean = false;
  isAdmin: boolean = false;

  constructor(
    private route: Router,
    private authenticationService: AuthenticationService,
    private categoryService: CategoryService) {
  }

  ngOnInit(): void {
    //Get category list and sort it by id value
    this.categories = this.categoryService.getCategories().pipe(map((items) => {
      items.sort((a, b) => {
        return a.id < b.id ? -1 : 1;
      });
      return items;
    }));

    //Get auth & role
    [this.isAuthenticated, this.isAdmin] = this.authenticationService.getGlobalAuth();
  }

  onCategoryDetailsClick(id: number) {
    this.route.navigate(['categories/' + id]);
  }

  onCategoryUpdateClick(id: number) {
    this.route.navigate(['categories/' + id + "/update"]);
  }

  onCategoryDeleteClick(id: number) {
    this.categoryService.deleteCategory(id).subscribe()
    window.alert('Category deleted !');
    location.reload(); //Refresh page to update changes
  }
}
