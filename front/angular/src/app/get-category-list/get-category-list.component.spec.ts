import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GetCategoryListComponent } from './get-category-list.component';

describe('GetCategoryListComponent', () => {
  let component: GetCategoryListComponent;
  let fixture: ComponentFixture<GetCategoryListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GetCategoryListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GetCategoryListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
