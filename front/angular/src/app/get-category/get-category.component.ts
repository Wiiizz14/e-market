import {Component, OnInit} from '@angular/core';
import {Category} from "../../interfaces/Category";
import {CategoryService} from "../../services/category.service";
import {ActivatedRoute, Router} from "@angular/router";
import {Observable} from "rxjs";
import {map} from "rxjs/operators";
import {ProductService} from "../../services/product.service";
import {AuthenticationService} from "../../services/helper/authentication.service";


@Component({
  selector: 'app-get-category',
  templateUrl: './get-category.component.html',
  styleUrls: ['./get-category.component.css']
})
export class GetCategoryComponent implements OnInit {

  category!: Observable<Category>;

  isAuthenticated: boolean = false;
  isAdmin: boolean = false;

  constructor(
    private route: ActivatedRoute,
    private routeRedir: Router,
    private authenticationService: AuthenticationService,
    private productService: ProductService,
    private categoryService: CategoryService) {
  }

  ngOnInit(): void {
    this.route.params.subscribe(routeParam => {
      this.category = this.categoryService.getCategoryById(routeParam.id).pipe(map((category) => {
        category.products.sort((a, b) => {
          return a.id < b.id ? -1 : 1;
        });
        return category;
      }));
    });

    //Get auth & role
    [this.isAuthenticated, this.isAdmin] = this.authenticationService.getGlobalAuth();
  }

  onProductDetailsClick(id: number) {
    this.routeRedir.navigate(['products/' + id]);
  }

  onProductUpdateClick(id: number) {
    this.routeRedir.navigate(['products/' + id + "/update"]);
  }

  onProductDeleteClick(id: number) {
    if (this.isAdmin) {
      this.productService.deleteProduct(id).subscribe();
      window.alert('Product deleted !');
      location.reload(); //Refresh page to update changes
    } else {
      window.alert("You are not administrator !");
      this.routeRedir.navigate(['']);
    }
  }
}
