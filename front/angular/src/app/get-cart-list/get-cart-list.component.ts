import { Component, OnInit } from '@angular/core';
import {Cart} from "../../interfaces/Cart";
import {Observable} from "rxjs";
import {CartService} from "../../services/cart.service";
import {map} from "rxjs/operators";
import {AuthenticationService} from "../../services/helper/authentication.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-get-cart-list',
  templateUrl: './get-cart-list.component.html',
  styleUrls: ['./get-cart-list.component.css']
})
export class GetCartListComponent implements OnInit {

  carts!: Observable<Cart[]>;

  isAuthenticated: boolean = false;
  isAdmin: boolean = false;

  constructor(
    private route: Router,
    private authenticationService: AuthenticationService,
    private cartService: CartService) {
  }

  ngOnInit(): void {
    //Get cart list and sort it by id value
    this.carts = this.cartService.getCarts().pipe(map((items) =>{
      items.sort((a, b) => {
        return a.id < b.id ? -1 : 1;
      });
      return items;
    }));

    //Get status
    [this.isAuthenticated, this.isAdmin] = this.authenticationService.getGlobalAuth();
  }

  onCartDetailsClick(id: number) {
    this.route.navigate(['carts/' + id]);
  }

  onCartDeleteClick(id: number) {
    this.cartService.deleteCart(id).subscribe();
    window.alert('Cart deleted');
    location.reload(); //Refresh page to update changes
  }
}
