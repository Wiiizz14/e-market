import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GetCartListComponent } from './get-cart-list.component';

describe('GetCartListComponent', () => {
  let component: GetCartListComponent;
  let fixture: ComponentFixture<GetCartListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GetCartListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GetCartListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
