import {Component, OnInit} from '@angular/core';
import {Observable} from "rxjs";
import {Product} from "../../interfaces/Product";
import {Page} from "../../interfaces/Page";
import {ProductService} from "../../services/product.service";
import {map} from "rxjs/operators";
import {Router} from "@angular/router";
import {CategoryService} from "../../services/category.service";
import {Category} from "../../interfaces/Category";
import {AuthenticationService} from "../../services/helper/authentication.service";

@Component({
  selector: 'app-product-list',
  templateUrl: './get-product-list.component.html',
  styleUrls: ['./get-product-list.component.css']
})
export class GetProductListComponent implements OnInit {

  obsPage!: Observable<Page<Product>>
  obsProducts!: Observable<Product[]>;
  obsCategories!: Observable<Category[]>;

  products!: Product[];
  categories!: Category[];

  isAuthenticated: boolean = false;
  isAdmin: boolean = false;

  constructor(
    private route: Router,
    private authenticationService: AuthenticationService,
    private productService: ProductService,
    private categoryService: CategoryService) {
  }

  displayPageContent(pageToDisplay: number) {
    //Get new page to display
    this.obsPage = this.productService.getProductPage(pageToDisplay);

    //Get obs list
    this.getProductListContent();

    //Get status
    [this.isAuthenticated, this.isAdmin] = this.authenticationService.getGlobalAuth();
  }

  ngOnInit(): void {
    //Get obs page
    this.obsPage = this.productService.getProductList();

    //Get obs list
    this.getProductListContent();

    //Get status
    [this.isAuthenticated, this.isAdmin] = this.authenticationService.getGlobalAuth();
  }

  onProductDetailsClick(id: number) {
    this.route.navigate(['products/' + id]);
  }

  onProductUpdateClick(id: number) {
    this.route.navigate(['products/' + id + "/update"]);
  }

  onProductDeleteClick(id: number) {
    this.productService.deleteProduct(id).subscribe();
    window.alert('Product deleted !');
    location.reload(); //Refresh page to update changes
  }

  private getProductListContent() {
    this.obsProducts = this.obsPage.pipe(map(page => page.content));
    this.obsCategories = this.categoryService.getCategories();
  }
}
