import { Component, OnInit } from '@angular/core';
import {Observable} from "rxjs";
import {User} from "../../interfaces/User";
import {ActivatedRoute} from "@angular/router";
import {UserService} from "../../services/user.service";
import {AuthenticationService} from "../../services/helper/authentication.service";

@Component({
  selector: 'app-get-user',
  templateUrl: './get-user.component.html',
  styleUrls: ['./get-user.component.css']
})
export class GetUserComponent implements OnInit {

  user!: Observable<User>;

  constructor(private route: ActivatedRoute, private userService: UserService, private authService: AuthenticationService) {
  }

  ngOnInit(): void {
    //Get current user id
    this.authService.getCurrentUser().subscribe((user) => {
      this.user = this.userService.getUserById(user.id);
    });
  }
}
